import inspect
from unittest import mock

import pytest

from xyzzanie import formatting
from xyzzanie import glk


def test_special_keys():
    special_keys = frozenset([
        "left",
        "right",
        "up",
        "down",
        "return",
        "delete",
        "escape",
        "tab",
        "pageup",
        "pagedown",
        "home",
        "end",
        "func1",
        "func2",
        "func3",
        "func4",
        "func5",
        "func6",
        "func7",
        "func8",
        "func9",
        "func10",
        "func11",
        "func12"
    ])
    assert glk.special_keys == special_keys


def test_error_event():
    event = glk.error_event("An error occured.")
    assert event == {"type": "error", "message": "An error occured."}


class TestDisplay:
    """Tests for the `Display` class."""

    def test_init(self):
        grids = ("a", "b")
        buffers = [[formatting.Text("Hello World", formatting.Style.NORMAL)]]
        display = glk.Display(grids=grids, buffers=buffers)
        assert display.grid_windows == grids
        assert display.buffer_windows == buffers

    @mock.patch("xyzzanie.formatting.split_message")
    def test_to_split_message_calls_split_message(self, mocked_split_message):
        display = glk.Display(grids=(), buffers=())
        display.to_split_message(1000)
        mocked_split_message.assert_called_once()

    def test_to_split_message_1_grid(self):
        grids = ("a",)
        buffers = ()
        display = glk.Display(grids=grids, buffers=buffers)
        assert display.to_split_message(1000) == ["```\na\n```"]

    def test_to_split_message_2_grids(self):
        grids = ("a", "b")
        buffers = ()
        display = glk.Display(grids=grids, buffers=buffers)
        assert display.to_split_message(1000) == ["```\na\n```\n```\nb\n```"]

    def test_to_split_message_1_buffer(self):
        grids = ()
        buffers = [[formatting.Text("Hello World", formatting.Style.NORMAL)]]
        display = glk.Display(grids=grids, buffers=buffers)
        assert display.to_split_message(1000) == ["Hello World"]

    def test_to_split_message_2_buffers(self):
        grids = ()
        buffers = [
            [formatting.Text("Hello", formatting.Style.NORMAL)],
            [formatting.Text("World", formatting.Style.NORMAL)]
        ]
        display = glk.Display(grids=grids, buffers=buffers)
        assert display.to_split_message(1000) == ["Hello\n---\nWorld"]

    def test_to_split_message_1_grid_1_buffer(self):
        grids = ("a",)
        buffers = [[formatting.Text("Hello World", formatting.Style.NORMAL)]]
        display = glk.Display(grids=grids, buffers=buffers)
        assert display.to_split_message(1000) == ["```\na\n```\nHello World"]

    def test_to_split_message_1_buffer_with_hyperlink(self):
        grids = ()
        buffers = [[formatting.Text("Hello World", formatting.Style.NORMAL, hyperlink=1)]]
        display = glk.Display(grids=grids, buffers=buffers)
        assert display.to_split_message(1000) == ["__Hello World__[1]"]

    def test_to_split_message_1_buffer_with_hyperlinks_disabled(self):
        grids = ()
        buffers = [[formatting.Text("Hello World", formatting.Style.NORMAL, hyperlink=1)]]
        display = glk.Display(grids=grids, buffers=buffers)
        assert display.to_split_message(1000, hyperlinks=False) == ["Hello World"]


class TestWindow:
    """Tests for the `Window` class."""

    def test_repr(self):
        window = glk.Window(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        assert repr(window) == "Window(id_=1)"

    def test_signature(self):
        params = inspect.signature(glk.Window).parameters
        expected_params = ["id_", "rock", "left", "top", "width", "height"]
        assert len(params) == len(expected_params)
        for name in expected_params:
            assert name in params
            assert params[name].kind == inspect.Parameter.KEYWORD_ONLY
            assert params[name].default == inspect.Parameter.empty

    def test_init(self):
        window = glk.Window(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        assert window.id == 1
        assert window.rock == 729
        assert window.left == 0
        assert window.top == 25
        assert window.width == 80
        assert window.height == 100

    def test_update(self):
        window = glk.Window(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        window.update(
            left=1,
            top=26,
            width=81,
            height=101
        )
        assert window.left == 1
        assert window.top == 26
        assert window.width == 81
        assert window.height == 101


class TestBufferWindow:
    """Tests for the `BufferWindow` class."""

    def test_max_content_length(self):
        assert glk.BufferWindow.MAX_CONTENT_LENGTH == 5000000

    def test_repr(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        assert repr(window) == "BufferWindow(id_=1)"

    def test_signature(self):
        params = inspect.signature(glk.BufferWindow).parameters
        expected_params = ["id_", "rock", "left", "top", "width", "height"]
        assert len(params) == len(expected_params)
        for name in expected_params:
            assert name in params
            assert params[name].kind == inspect.Parameter.KEYWORD_ONLY
            assert params[name].default == inspect.Parameter.empty

    def test_init(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        assert window.id == 1
        assert window.rock == 729
        assert window.left == 0
        assert window.top == 25
        assert window.width == 80
        assert window.height == 100
        assert window.content == []
        assert window.content_length == 0
        assert window.pending_content == []
        assert window.pending_content_length == 0
        assert window.hyperlinks == set()

    def test_update(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        window.update(
            left=1,
            top=26,
            width=81,
            height=101
        )
        assert window.left == 1
        assert window.top == 26
        assert window.width == 81
        assert window.height == 101

    def test_queue_content_when_window_empty(self):
        """There never is an initial newline."""

        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )

        # Without the append key.
        text = [{"content": [{"style": "normal", "text": "Hello World"}]}]
        window.queue_content(text)
        expected = [formatting.Text("Hello World", formatting.Style.NORMAL)]
        assert window.pending_content == expected

        # With append set to True.
        window.pending_content = []
        text[0]["append"] = True
        window.queue_content(text)
        expected = [formatting.Text("Hello World", formatting.Style.NORMAL)]
        assert window.pending_content == expected

        # With append set to False.
        window.pending_content = []
        text[0]["append"] = False
        window.queue_content(text)
        expected = [formatting.Text("Hello World", formatting.Style.NORMAL)]
        assert window.pending_content == expected

    def test_queue_content_when_content_non_empty(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        window.content = [formatting.Text("Hi!", formatting.Style.NORMAL)]

        # Without the append key, there should be a new line.
        text = [{"content": [{"style": "normal", "text": "Hello World"}]}]
        window.queue_content(text)
        expected = [
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("Hello World", formatting.Style.NORMAL)
        ]
        assert window.pending_content == expected

        # With append set to True, there should not be a new line.
        window.pending_content = []
        text[0]["append"] = True
        window.queue_content(text)
        expected = [formatting.Text("Hello World", formatting.Style.NORMAL)]
        assert window.pending_content == expected

        # With append set to False, there should be a new line.
        window.pending_content = []
        text[0]["append"] = False
        window.queue_content(text)
        expected = [
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("Hello World", formatting.Style.NORMAL)
        ]
        assert window.pending_content == expected

    def test_queue_content_when_pending_content_non_empty(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )

        # Without the append key, there should be a new line.
        window.pending_content = [formatting.Text("Hello ", formatting.Style.NORMAL)]
        text = [{"content": [{"style": "normal", "text": "World"}]}]
        window.queue_content(text)
        expected = [
            formatting.Text("Hello ", formatting.Style.NORMAL),
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("World", formatting.Style.NORMAL)
        ]
        assert window.pending_content == expected

        # With append set to True, there should not be a new line.
        window.pending_content = [formatting.Text("Hello ", formatting.Style.NORMAL)]
        text[0]["append"] = True
        window.queue_content(text)
        expected = [
            formatting.Text("Hello ", formatting.Style.NORMAL),
            formatting.Text("World", formatting.Style.NORMAL)
        ]
        assert window.pending_content == expected

        # With append set to False, there should be a new line.
        window.pending_content = [formatting.Text("Hello ", formatting.Style.NORMAL)]
        text[0]["append"] = False
        window.queue_content(text)
        expected = [
            formatting.Text("Hello ", formatting.Style.NORMAL),
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("World", formatting.Style.NORMAL)
        ]
        assert window.pending_content == expected

    def test_content_lengths(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        window.queue_content([
            {"content": [{"style": "normal", "text": "Hello World"}]}
        ])
        assert window.pending_content_length == 11
        window.get_pending_content()
        assert window.pending_content_length == 0
        assert window.content_length == 11

    def test_queue_multiple_lines(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        text = [
            {"content": [{"style": "normal", "text": "Hello"}]},
            {"content": [{"style": "normal", "text": "World"}]},
            {"content": {}},
            {"content": [{"style": "normal", "text": "New paragraph"}]}
        ]
        window.queue_content(text)
        expected = [
            formatting.Text("Hello", formatting.Style.NORMAL),
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("World", formatting.Style.NORMAL),
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("New paragraph", formatting.Style.NORMAL)
        ]
        assert window.pending_content == expected

    def test_queue_multiple_runs_in_line(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        text = [
            {"content": [
                {"style": "normal", "text": "Hello "},
                {"style": "normal", "text": "World"}
            ]}]
        window.queue_content(text)
        expected = [
            formatting.Text("Hello ", formatting.Style.NORMAL),
            formatting.Text("World", formatting.Style.NORMAL)
        ]
        assert window.pending_content == expected

    def test_queue_styled_text(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )

        text = [
            {"content": [
                {"style": "normal", "text": "Normal"},
                {"style": "normal", "text": " "},
                {"style": "emphasized", "text": "Emphasized"},
                {"style": "normal", "text": " "},
                {"style": "preformatted", "text": "Preformatted"},
                {"style": "normal", "text": " "},
                {"style": "header", "text": "Header"},
                {"style": "normal", "text": " "},
                {"style": "subheader", "text": "Subheader"},
                {"style": "normal", "text": " "},
                {"style": "alert", "text": "Alert"},
                {"style": "normal", "text": " "},
                {"style": "note", "text": "Note"},
                {"style": "normal", "text": " "},
                {"style": "blockquote", "text": "Blockquote"},
                {"style": "normal", "text": " "},
                {"style": "input", "text": "Input"},
                {"style": "normal", "text": " "},
                {"style": "user1", "text": "User 1"},
                {"style": "normal", "text": " "},
                {"style": "user2", "text": "User 2"}
            ]}]
        expected = [
            formatting.Text("Normal", formatting.Style.NORMAL),
            formatting.Text(" ", formatting.Style.NORMAL),
            formatting.Text("Emphasized", formatting.Style.EMPHASIZED),
            formatting.Text(" ", formatting.Style.NORMAL),
            formatting.Text("Preformatted", formatting.Style.PREFORMATTED),
            formatting.Text(" ", formatting.Style.NORMAL),
            formatting.Text("Header", formatting.Style.HEADER),
            formatting.Text(" ", formatting.Style.NORMAL),
            formatting.Text("Subheader", formatting.Style.SUBHEADER),
            formatting.Text(" ", formatting.Style.NORMAL),
            formatting.Text("Alert", formatting.Style.ALERT),
            formatting.Text(" ", formatting.Style.NORMAL),
            formatting.Text("Note", formatting.Style.NOTE),
            formatting.Text(" ", formatting.Style.NORMAL),
            formatting.Text("Blockquote", formatting.Style.BLOCKQUOTE),
            formatting.Text(" ", formatting.Style.NORMAL),
            formatting.Text("Input", formatting.Style.INPUT),
            formatting.Text(" ", formatting.Style.NORMAL),
            formatting.Text("User 1", formatting.Style.USER1),
            formatting.Text(" ", formatting.Style.NORMAL),
            formatting.Text("User 2", formatting.Style.USER2)
        ]
        window.queue_content(text)
        assert window.pending_content == expected

    def test_get_pending_content(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        window.content = [formatting.Text("Hello", formatting.Style.NORMAL)]
        window.pending_content = [
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("World", formatting.Style.NORMAL)
        ]
        new_content = window.get_pending_content()
        assert new_content == [
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("World", formatting.Style.NORMAL)
        ]
        assert window.content == [
            formatting.Text("Hello", formatting.Style.NORMAL),
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("World", formatting.Style.NORMAL)
        ]
        assert window.pending_content == []

    def test_clear(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        window.content = [formatting.Text("Hello World", formatting.Style.NORMAL)]
        window.content_length = 11
        window.hyperlinks = {729}
        window.clear()
        assert window.content == []
        assert window.content_length == 0
        assert window.hyperlinks == set()

    def test_content_cannot_exceed_max_content_length(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        window.content = [formatting.Text(
            "x" * glk.BufferWindow.MAX_CONTENT_LENGTH,
            formatting.Style.NORMAL
        )]
        window.content_length = glk.BufferWindow.MAX_CONTENT_LENGTH
        window.pending_content = [
            formatting.Text("\n", formatting.Style.NORMAL),
            formatting.Text("Hello World", formatting.Style.NORMAL)
        ]
        window.pending_content_length = 12
        window.get_pending_content()
        assert window.content == []


class TestGridWindow:
    """Tests for the `GridWindow` class."""

    def test_repr(self):
        window = glk.GridWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100,
            grid_height=3,
            grid_width=30
        )
        assert repr(window) == "GridWindow(id_=1)"

    def test_signature(self):
        params = inspect.signature(glk.GridWindow).parameters
        expected_params = [
            "id_",
            "rock",
            "left",
            "top",
            "width",
            "height",
            "grid_height",
            "grid_width"
        ]
        assert len(params) == len(expected_params)
        for name in expected_params:
            assert name in params
            assert params[name].kind == inspect.Parameter.KEYWORD_ONLY
            assert params[name].default == inspect.Parameter.empty

    def test_init(self):
        window = glk.GridWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100,
            grid_height=3,
            grid_width=30
        )
        assert window.id == 1
        assert window.rock == 729
        assert window.left == 0
        assert window.top == 25
        assert window.width == 80
        assert window.height == 100
        assert window.grid_height == 3
        assert window.grid_width == 30
        assert window.lines == {}

    def test_update(self):
        window = glk.GridWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100,
            grid_height=3,
            grid_width=30
        )
        window.update(
            left=1,
            top=26,
            width=81,
            height=101,
            grid_height=4,
            grid_width=31
        )
        assert window.left == 1
        assert window.top == 26
        assert window.width == 81
        assert window.height == 101
        assert window.grid_height == 4
        assert window.grid_width == 31

    def test_add_lines(self):
        window = glk.GridWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100,
            grid_height=3,
            grid_width=30
        )
        lines = [
            {"line": 0, "content": [{"style": "normal", "text": "Hello"}]},
            {"line": 2, "content": [{"style": "alert", "text": "World"}]}
        ]
        window.update_content(lines)
        assert window.lines == {0: "Hello", 2: "World"}

    def test_remove_line(self):
        window = glk.GridWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100,
            grid_height=3,
            grid_width=30
        )
        window.lines == {0: "Hello", 1: "World"}
        lines = [
            {"line": 0, "content": [{"style": "normal", "text": "Hello"}]},
            {"line": 1, "content": {}}
        ]
        window.update_content(lines)
        assert window.lines == {0: "Hello"}

    def test_get_content(self):
        window = glk.GridWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100,
            grid_height=3,
            grid_width=30
        )

        # We start with 3 empty lines.
        assert window.get_content() == "\n\n"

        # With an empty line in the middle.
        window.lines = {0: "Hello", 2: "World"}
        assert window.get_content() == "Hello\n\nWorld"

        # With a very long line that should be truncated.
        window.lines = {0: "Hello", 1: "World", 2: "VeryLong" * 50}
        expected_second_line = ("VeryLong" * 300)[:window.grid_width]
        assert window.get_content() == "Hello\nWorld\n" + expected_second_line


class TestCreateWindow:
    """Tests for the `create_window` function"""

    def test_no_type(self):
        with pytest.raises(KeyError):
            glk.create_window({
                "id": 1,
                "rock": 729,
                "left": 0,
                "top": 25,
                "width": 80,
                "height": 100
            })

    def test_wrong_type(self):
        with pytest.raises(ValueError):
            glk.create_window({
                "type": "special",
                "id": 1,
                "rock": 729,
                "left": 0,
                "top": 25,
                "width": 80,
                "height": 100
            })

    def test_create_buffer(self):
        window = glk.create_window({
            "type": "buffer",
            "id": 1,
            "rock": 729,
            "left": 0,
            "top": 25,
            "width": 80,
            "height": 100
        })
        assert isinstance(window, glk.BufferWindow)
        assert window.id == 1
        assert window.rock == 729
        assert window.left == 0
        assert window.top == 25
        assert window.width == 80
        assert window.height == 100

    def test_create_grid(self):
        window = glk.create_window({
            "type": "grid",
            "id": 1,
            "rock": 729,
            "left": 0,
            "top": 25,
            "width": 80,
            "height": 100,
            "gridheight": 3,
            "gridwidth": 30
        })
        assert isinstance(window, glk.GridWindow)
        assert window.id == 1
        assert window.rock == 729
        assert window.left == 0
        assert window.top == 25
        assert window.width == 80
        assert window.height == 100
        assert window.grid_height == 3
        assert window.grid_width == 30


class TestUpdateWindow:
    """Tests for the `update_window` function"""

    def test_wrong_type(self):
        with pytest.raises(TypeError):
            glk.update_window("not a window", {
                "type": "special",
                "id": 1,
                "rock": 729,
                "left": 0,
                "top": 25,
                "width": 80,
                "height": 100
            })

    def test_update_buffer(self):
        window = glk.BufferWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100
        )
        glk.update_window(window, {
            "left": 1,
            "top": 26,
            "width": 81,
            "height": 101,
        })
        assert window.left == 1
        assert window.top == 26
        assert window.width == 81
        assert window.height == 101

    def test_update_grid(self):
        window = glk.GridWindow(
            id_=1,
            rock=729,
            left=0,
            top=25,
            width=80,
            height=100,
            grid_height=3,
            grid_width=30
        )
        glk.update_window(window, {
            "left": 1,
            "top": 26,
            "width": 81,
            "height": 101,
            "gridheight": 4,
            "gridwidth": 31
        })
        assert window.left == 1
        assert window.top == 26
        assert window.width == 81
        assert window.height == 101
        assert window.grid_height == 4
        assert window.grid_width == 31
