import enum
from typing import Iterable, List, Optional

from discord.utils import escape_markdown, escape_mentions


markdown_styling = {
    # No styling
    "x_none": {"start": "", "end": ""},
    # Code block.
    "x_grid": {"start": "```\n", "end": "\n```"},
    # Diff code block with a plus for green text.
    "x_info": {"start": "```diff\n", "end": "\n```", "middle": "+ "},
    # Diff code block with a minus for red text.
    "x_error": {"start": "```diff\n", "end": "\n```", "middle": "- "},
    # Markdown code block.
    "x_markdown_block": {"start": "```markdown\n", "end": "\n```"},
    # No styling.
    "normal": {"start": "", "end": ""},
    # Italic.
    "emphasized": {"start": "*", "end": "*"},
    # Inline code.
    "preformatted": {"start": "`", "end": "`"},
    # Bold underlined.
    "header": {"start": "__**", "end": "**__"},
    # Bold.
    "subheader": {"start": "**", "end": "**"},
    # Bold italic.
    "alert": {"start": "***", "end": "***"},
    # Underlined.
    "note": {"start": "__", "end": "__"},
    # Code block. TODO: Is code block appropriate?
    "blockquote": {"start": "```\n", "end": "\n```"},
    # Bold.
    "input": {"start": "**", "end": "**"},
    # No styling.
    "user1": {"start": "", "end": ""},
    # No styling.
    "user2": {"start": "", "end": ""},
    # Code block.
    "grid": {"start": "```\n", "end": "\n```"},
    # Underline.
    "hyperlink": {"start": "__", "end": "__"},
}


class Style(enum.Enum):
    """An enumeration of all the styles the bot can use.

    Xyzzanie-specific styles are prefixed with `X_`. Glk styles aren't
    prefixed.

    In the case of Glk styles, the values are strings corresponding to those
    used by RemGlk.
    """

    # Unstyled text.
    X_NONE = "x_none"
    # Used for whole grid windows and other preformatted text.
    X_GRID = "x_grid"
    # Xyzzanie info messages.
    X_INFO = "x_info"
    # Xyzzanie error messages.
    X_ERROR = "x_error"
    # Used when listing stories.
    X_MARKDOWN_BLOCK = "x_markdown_block"

    # And now the Glk styles.
    NORMAL = "normal"
    EMPHASIZED = "emphasized"
    PREFORMATTED = "preformatted"
    HEADER = "header"
    SUBHEADER = "subheader"
    ALERT = "alert"
    NOTE = "note"
    BLOCKQUOTE = "blockquote"
    INPUT = "input"
    USER1 = "user1"
    USER2 = "user2"
    # For Glk hyperlinks. (Not a Glk style per se, but anyway.)
    # TODO: I don't think it's used anymore.
    HYPERLINK = "hyperlink"


class Text:
    """A run of text with the same Glk style."""

    def __init__(self, content: str, style: Style, hyperlink: Optional[int] = None):
        self.content = content
        self.style = style
        self.hyperlink = hyperlink

    def __repr__(self):
        return f"Text({self.content!r}, {self.style!s}, hyperlink={self.hyperlink!r})"

    def __str__(self):
        return self.content

    def __eq__(self, other):
        return isinstance(other, Text) and self.content == other.content and self.style is other.style and self.hyperlink == other.hyperlink

    def to_markdown(self, hyperlink=True):
        """Return the content of the Text styled with Markdown."""
        text = to_markdown(self.content, self.style)
        if hyperlink and self.hyperlink is not None:
            text = "__" + text + f"__[{self.hyperlink!r}]"
        return text


def to_markdown(text: str, style: Style):
    """Format the given text with Markdown according to `style`."""

    delimiters = markdown_styling[style.value]

    # Don't escape the text if the style is preformatted (i.e starts with "`").
    # TODO: Is it the right place to escape mentions?
    if not delimiters["start"].startswith("`"):
        text = escape_markdown(escape_mentions(text))
    if "middle" in delimiters:
        lines = text.split("\n")
        prefixed_lines = []
        for line in lines:
            if line:
                line = delimiters["middle"] + line
            prefixed_lines.append(line)
        text = "\n".join(prefixed_lines)

    return delimiters["start"] + text + delimiters["end"]


def split_message(msg: Iterable[Text], max_len: int, hyperlinks=True) -> List[str]:
    """Transform a list of `Text`s into a list of formatted strings.

    If `hyperlinks` is True, `Text`s with the `hyperlink` attribute will have
    additional styling.

    Each element of the returned list will have a length of `max_len`, except
    for the last one which can be shorter.

    If the content of a `Text` is split into multiple elements, its style will
    be properly closed at the end of the first element and reopened at the
    start of the second.

    Raises `ValueError` if `max_len` is too short to place at least a
    character in addition to the styling.
    """

    # TODO:
    # - Doesn't fully take account of styles with a "middle" delimiter (X_INFO
    #   and X_ERROR): it only adds the middle delimiter at the start of a
    #   chunk. That's not a big deal though, since texts with this style are
    #   short messages.
    # - A Text can be split just after or before a space, which will cause the
    #   Markdown not to render. (E.g. "*Hello *" won't be in italic.)

    chunks = []
    current_chunk = ""

    for txt in msg:
        start_delimiter = markdown_styling[txt.style.value]["start"]
        if txt.hyperlink is not None and hyperlinks:
            start_delimiter = markdown_styling["hyperlink"]["start"] + start_delimiter
        middle_delimiter = markdown_styling[txt.style.value].get("middle", "")
        end_delimiter = markdown_styling[txt.style.value]["end"]
        if txt.hyperlink is not None and hyperlinks:
            end_delimiter = end_delimiter + markdown_styling["hyperlink"]["end"] + f"[{txt.hyperlink!r}]"

        # Don't escape the text if the style is preformatted (i.e starts with "`").
        # TODO: Is it the right place to escape mentions?
        if not start_delimiter.startswith("`"):
            remaining = escape_markdown(escape_mentions(txt.content))
        else:
            remaining = txt.content

        while remaining:
            # Plus 1 so that there is at least place for one character.
            if len(current_chunk + start_delimiter + middle_delimiter + end_delimiter) + 1 > max_len:
                chunks.append(current_chunk)
                current_chunk = ""
            available_length = max_len - len(current_chunk + start_delimiter + middle_delimiter + end_delimiter)
            if available_length <= 0:
                raise ValueError(
                    "The max length given is too short for the message's content."
                )
            current_chunk += start_delimiter + middle_delimiter + remaining[:available_length] + end_delimiter
            remaining = remaining[available_length:]

    if current_chunk:
        chunks.append(current_chunk)
    return chunks
