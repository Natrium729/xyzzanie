en = {
    # Playing status when playing two apps.
    "playing_status_two_apps": "${last_app} and 1 other",

    # Playing status when playing more than two apps.
    "playing_status_more_apps": "${last_app} and ${other_number} others",

    # The bot returned no reply to a command.
    "no_reply_returned": "An error has occured. Please try something else.",

    # An empty meta command was sent to the bot.
    "meta_command_empty": 'Please type a command after "${meta_prompt}". For available commands, type "${meta_prompt}help".',

    # A meta command didn't return a reply.
    "meta_command_no_reply": "The command was executed, but no reply has been returned.",

    # Invalid meta command.
    "meta_command_invalid": 'The command "${command}" is not valid. For available commands, type "${meta_prompt}help".',

    # A story command is sent when no story is being played.
    "story_not_playing": 'No story is currently being played. Please type "${meta_prompt}list" to list stories available, and "${meta_prompt}play [filename]" to play the specified story.',

    # A story command is sent when the story is stopped.
    "story_is_stopped": 'ERROR: The story is stopped. Type "${meta_prompt}forcequit" to remove it so that you can play another story.',

    # An input is sent to a story that awaits only hyperlinks.
    "story_only_hyperlinks": 'The story is only awaiting hyperlinks. Type "${meta_prompt}link [number]" to click a hyperlink.',

    # An input is sent to a story that awaits none.
    "story_no_inputs": "ERROR: The story is not awaiting any input.",

    # The help text.
    "help": 'Type "${meta_prompt}list" to get the available stories and "${meta_prompt}play [filename]" to play the specified story.\nYou can then interact with the story by prefixing your command by "${story_prompt}".\nFor some informations about the bot, type "${meta_prompt}about".\nFor detailed help, other commands and source code, please visit <https://gitlab.com/Natrium729/xyzzanie>.',

    # The about text.
    "about": "Xyzzanie is a Discord bot written by Nathanaël Marion (a.k.a Natrium729).\nFor more info, please visit <https://gitlab.com/Natrium729/xyzzanie>.",

    # The story list has been refreshed.
    "story_list_refreshed": "The story list has been refreshed.",

    # The story folder doesn't exist.
    "story_folder_missing": "ERROR: The story folder doesn't exist.",

    # The story folder cannot be opened.
    "story_folder_unopenable": "ERROR: The story folder could not be opened.",

    # The title when listing stories.
    "story_list_title": "Stories available",

    # A story is already being played when trying to start a new one.
    "story_already_playing": 'The story "${story}" is currently being played. Quit it before loading another story. (You can also forcequit the current story by typing "${meta_prompt}forcequit".)',

    # No story specified when trying to start one.
    "no_story_given": 'Please provide a story file. (Type "${meta_prompt}list" to get the stories available.)',

    # The story is not in the list of stories.
    "story_not_in_list": 'The story file "${filename}" could not be found. Please type "${meta_prompt}list" to get the stories available.',

    # The story file could not be opened.
    "story_unopenable": 'ERROR: The story file "${filename}" could not be opened.',

    # The interpreter couldn't be launched.
    "interpreter_unlaunchable": "ERROR: The interpreter could not be launched.",

    # The story file doesn't exist.
    "story_file_nonexistent": 'The story file "${filename}" does not exist. Please type "${meta_prompt}refresh" to refresh the story list.',

    # Link command with no arguments or an invalid arg.
    "link_invalid_arg": 'Please provide a number with the command to click the link with that ID in the current story. You can also use "${meta_prompt}link off" and "${meta_prompt}link on" to stop or start indicating them in stories.',

    # Links are enabled.
    "link_enable": "Links will now be indicated in the stories.",

    # Links are disabled.
    "link_disable": "Links won't be indicated in the stories.",

    # Invalid link value.
    "link_invalid_value": "The story has no link with this value available.",

    # A story is forcequit.
    "forcequit": "The current story has been stopped.",

    # Forcequit when no story is playing.
    "forcequit_without_story": "No story is currently being played.",

    # A story is awaiting several inputs
    "story_with_several_inputs": "The story is awaiting several inputs, but Xyzzanie only supports one input at a time. The story has been stopped.",

    # A story closed all its windows.
    "story_all_windows_closed": "The story has closed all its windows and has been stopped.",

    # A story ends.
    "story_ended": 'The story "${story}" has ended.',
}
