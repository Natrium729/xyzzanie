import json
import logging

from xyzzanie import config

logger = logging.getLogger("xyzzanie.secrets")

token = ""
token_dev = ""


def update():
    """Read the secrets.json file and load the secret Discord client token.

    If there are errors when reading the file, they are ignored and the token
    will be emtpy, so that we can simulate the bot without a token.

    (The token is checked in `xyzzanie.bot.run` so that we cannot launch the
    bot without it.)
    """

    global token
    global token_dev

    secrets_path = config.root_path / "secrets.json"

    # Load secrets file.
    secrets = {}
    logger.info(f"Opening secrets file at <{secrets_path}>.")
    if secrets_path.exists():
        try:
            with secrets_path.open() as f:
                secrets = json.load(f)
        except OSError:
            logger.warning(
                "The secrets file cannot be opened. Using an empty token.",
                exc_info=True
            )
        except json.JSONDecodeError:
            logger.warning(
                "The secret file is malformed. Using an empty token.",
                exc_info=True
            )
    else:
        logger.warning("No secrets file found. Using an empty token.")

    # The token.
    token = str(secrets.get("token", ""))
    # The dev token.
    token_dev = str(secrets.get("token-dev", ""))

    logger.info("Secrets loaded.")
