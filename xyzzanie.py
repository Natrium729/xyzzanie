import argparse
import sys

import xyzzanie.loggingsetup  # noqa: F401
from xyzzanie import apps
from xyzzanie import bot
from xyzzanie import config
from xyzzanie import secrets


def run(args):
    bot.run(dev=args.dev)


def simulate(args):
    bot.simulate(dev=args.dev)


def main(args=None):
    """The command line interface, where `args` contains the command line
    arguments."""

    parser = argparse.ArgumentParser(
        description="A Discord bot to play parser interactive fiction"
    )
    parser.add_argument(
        "-D", "--dev",
        help="use the development mode",
        action="store_true"
    )
    subparsers = parser.add_subparsers()
    parser_run = subparsers.add_parser(
        "run",
        aliases=["r"],
        help="run the Discord bot"
    )
    parser_run.set_defaults(func=run)
    parser_simulate = subparsers.add_parser(
        "simulate",
        aliases=["sim", "s"],
        help="simulate the Discord bot in the terminal"
    )
    parser_simulate.set_defaults(func=simulate)
    args = parser.parse_args(args)

    config.update(dev=args.dev)
    secrets.update()

    # We load the story list at startup.
    apps.InterpreterApp.refresh_stories_available()

    if hasattr(args, "func"):
        args.func(args)
    else:
        parser.print_help()
        sys.exit()


if __name__ == "__main__":
    main()
