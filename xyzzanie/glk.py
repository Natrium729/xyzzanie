from typing import List, Sequence, Union

from xyzzanie import formatting


special_keys = frozenset([
    "left",
    "right",
    "up",
    "down",
    "return",
    "delete",
    "escape",
    "tab",
    "pageup",
    "pagedown",
    "home",
    "end",
    "func1",
    "func2",
    "func3",
    "func4",
    "func5",
    "func6",
    "func7",
    "func8",
    "func9",
    "func10",
    "func11",
    "func12"
])


def error_event(msg):
    return {"type": "error", "message": msg}


class Display:
    """Represents the contents of a Glk app to be shown.

    `grid_windows` holds a list of strings, and `buffer_windows` a list of
    lists of `formatting.Text`s.
    """

    def __init__(self, *, grids: Sequence[str], buffers: Sequence[Sequence[formatting.Text]]):
        self.grid_windows = grids
        self.buffer_windows = buffers
        # GRPHCS
        # self.graphics_windows = graphics

    def to_split_message(self, length: int, hyperlinks=True) -> List[str]:
        """Split the display's content using `formatting.split_message`

        The content of each grid window will have the `X_GRID` style applied.
        and will be separated by a line break.

        The content of the buffer windows will use the Glk styles of their
        content. Each one will be separated by "---".
        """

        texts = []
        for i, grid in enumerate(self.grid_windows):
            texts.append(formatting.Text(grid, formatting.Style.X_GRID))
            if i + 1 < len(self.grid_windows) or self.grid_windows and self.buffer_windows:
                texts.append(formatting.Text("\n", formatting.Style.X_NONE))
        for i, buffer in enumerate(self.buffer_windows):
            texts.extend(buffer)
            if i + 1 < len(self.buffer_windows):
                texts.append(formatting.Text("\n---\n", formatting.Style.X_NONE))
        return formatting.split_message(texts, length, hyperlinks=hyperlinks)


class Window:
    """Base class for the Glk windows."""

    def __repr__(self):
        return f"Window(id_={self.id!r})"

    def __init__(self, *, id_, rock, left, top, width, height):
        self.id = id_
        self.rock = rock
        self.left = left
        self.top = top
        self.width = width
        self.height = height
        self.hyperlinks = set()

    def update(self, *, left, top, width, height):
        """Update the position and size of the window."""
        self.left = left
        self.top = top
        self.width = width
        self.height = height


class BufferWindow(Window):
    """A Glk buffer window.

    The property `content` contains what has already been displayed by the
    window. The property `pending_content` contains what hasn't been shown yet.
    """

    # The content of a window cannot exceed this length.
    # (Size of a string this length: ~5 Mo, if I'm not mistaken.)
    # The pending content has unlimited size, but that's not a problem if we
    # retrieve it regularly.
    MAX_CONTENT_LENGTH = 5000000

    def __repr__(self):
        return f"BufferWindow(id_={self.id!r})"

    def __init__(self, *, id_, rock, left, top, width, height):
        super().__init__(
            id_=id_,
            rock=rock,
            left=left,
            top=top,
            width=width,
            height=height
        )

        self.content = []
        self.pending_content = []
        self.content_length = 0
        self.pending_content_length = 0

    def queue_content(self, text: list, clear=False):
        """Convert the list of Remglk content lines to a string to be shown later.

        If `clear` is `True`, `self.content` will be emptied first.

        The queued content can be retrieved with `self.get_pending_content`.
        """

        if clear:
            self.clear()

        for line in text:
            if not line.get("append", False) and (self.content or self.pending_content):
                self.pending_content.append(
                    formatting.Text("\n", formatting.Style.NORMAL)
                )
                self.pending_content_length += 1

            for run in line.get("content", {}):
                if "text" in run:
                    hyperlink = run.get("hyperlink", None)
                    run_text = formatting.Text(
                        run["text"],
                        formatting.Style(run["style"]),
                        hyperlink=hyperlink
                    )
                    self.pending_content.append(run_text)
                    self.pending_content_length += len(run_text.content)
                    if hyperlink is not None:
                        self.hyperlinks.add(hyperlink)

    def get_pending_content(self):
        """Add the pending content to `self.content`, clear it and return it.

        Also clears `self.content` if its length exceeds `MAX_CONTENT_LENGTH`.
        """

        self.content.extend(self.pending_content)
        self.content_length += self.pending_content_length
        if self.content_length > self.MAX_CONTENT_LENGTH:
            self.clear()
        output = self.pending_content
        self.pending_content = []
        self.pending_content_length = 0
        return output

    def clear(self):
        """Clear the content of the window."""
        # TODO: if self.pending_content is not empty, it won't be cleared too,
        # else we will never be able to show it! It is thus possible to have
        # text in self.content even if the window has been cleared after it,
        # if the window has been cleared several times before the pending
        # content is retrieved.
        self.content = []
        self.content_length = 0
        self.hyperlinks = set()


class GridWindow(Window):
    """A Glk grid window.

    The property `lines` contains the non-empty lines of the window.
    """

    def __repr__(self):
        return f"GridWindow(id_={self.id!s})"

    def __init__(self, *, id_, rock, left, top, width, height, grid_height, grid_width):
        super().__init__(
            id_=id_,
            rock=rock,
            left=left,
            top=top,
            width=width,
            height=height
        )

        self.grid_height = grid_height
        self.grid_width = grid_width
        self.lines = {}  # Only non-empty lines. Keys will be ints!

    def update(self, *, left, top, width, height, grid_height, grid_width):
        """Update the position and size of the window."""
        super().update(left=left, top=top, width=width, height=height)
        self. grid_height = grid_height
        self.grid_width = grid_width

    def update_content(self, lines):
        """Update the content of the window according to the Remglk lines
        given.
        """
        for line in lines:
            line_number = line["line"]
            self.lines[line_number] = ""
            for run in line.get("content", []):
                if "text" in run:
                    self.lines[line_number] += run["text"]
            if not self.lines[line_number]:
                del self.lines[line_number]

    def get_content(self):
        """Return a string containing all the windows lines, including
        non-empty ones."""
        line_list = []
        for i in range(self.grid_height):
            line_list.append(self.lines.get(i, "")[:self.grid_width])
        return "\n".join(line_list)


# GRPHCS
class GraphicsWindow(Window):
    pass


def create_window(d: dict) -> Union[BufferWindow, GridWindow]:
    """Helper function to create new windows.

    The argument is a dictionary similar to the objects of a RemGlk windows
    update array.

    It should contain a `"type"` key with a value of `"buffer"`, in which case
    a new `BufferWindow` is returned, or `"grid"`, in which case a new
    `GridWindow` is returned.

    The other keys should match the arguments of the created window's
    constructor, but with the RemGlk spelling (e.g. `"id"` instead of `"id_"`
    or `"gridwidth"` instead of `"grid_width"`).

    Other keys are ignored.
    """

    if d["type"] == "buffer":
        return BufferWindow(
            id_=d["id"],
            rock=d["rock"],
            left=d["left"],
            top=d["top"],
            width=d["width"],
            height=d["height"]
        )
    elif d["type"] == "grid":
        return GridWindow(
            id_=d["id"],
            rock=d["rock"],
            left=d["left"],
            top=d["top"],
            width=d["width"],
            height=d["height"],
            grid_height=d["gridheight"],
            grid_width=d["gridwidth"]
        )
    # GRPHCS
    # elif d["type"] == "graphics":
    #     pass
    else:
        raise ValueError(
            "The argument should have a key 'type' with the value 'buffer' or 'grid'."
        )


def update_window(window: Union[BufferWindow, GridWindow], d: dict):
    """Helper function to update windows.

    The first argument is the window to be updated.

    The second argument is a dictionary similar to the objects of a RemGlk
    windows update array.

    Its keys should match the arguments of the window's `update` method, but
    with the RemGlk spelling (e.g. `"id"` instead of `"id_"` or `"gridwidth"`
    instead of `"grid_width"`).

    Other keys are ignored.
    """

    if isinstance(window, BufferWindow):
        window.update(
            left=d["left"],
            top=d["top"],
            width=d["width"],
            height=d["height"]
        )
    elif isinstance(window, GridWindow):
        return window.update(
            left=d["left"],
            top=d["top"],
            width=d["width"],
            height=d["height"],
            grid_height=d["gridheight"],
            grid_width=d["gridwidth"]
        )
    # GRPHCS
    # elif isinstance(window, GraphicsWindow):
    #     pass
    else:
        raise TypeError(
            "The argument `window` should be of type `BufferWindow` or `GridWindow`."
        )
