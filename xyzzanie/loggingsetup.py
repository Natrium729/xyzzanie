import logging


# We set up the logger before importing Xyzzanie's modules so that logging at
# the root of the modules will work.
logger = logging.getLogger("xyzzanie")
logger.setLevel(logging.DEBUG)

# Create the console handler.
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

# create formatter and add it to the handlers
formatter = logging.Formatter(
    "[%(asctime)s] %(name)s %(levelname)s: %(message)s"
)
ch.setFormatter(formatter)

# Add the handlers to the logger.
logger.addHandler(ch)
