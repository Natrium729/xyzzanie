from xyzzanie import apps
from xyzzanie import glk


def line_input(gen, id_=0):
    """Return a Remglk line input update."""
    return {
        "id": id_,
        "gen": gen,
        "type": "line",
        "maxlen": 100
    }


def char_input(gen, id_=0):
    """Return a Remglk char input update."""
    return {
        "id": id_,
        "gen": gen,
        "type": "char"
    }


class SimpleApp(apps.BaseApp):
    """An app class only used to test the BaseApp class.

    It is needed because we need an `_accept` method to test some of the
    `BaseApp` functionalities.

    Its behavior is described in the docstring of its `_accept` method.
    """

    def __repr__(self):
        return f"SimpleApp(debug={self.debug!r})"

    def __init__(self, debug=False, autoinit=True):
        """If `init` is `True`, an GlkOte init event will be accepted on
        creation."""

        super().__init__(debug=debug)
        self.init = False
        self.hyperlink_id = 0  # The ID for the next hyperlink
        self.window_left = 0  # The left attribute of the window.
        if autoinit:
            self.accept({
                "type": "init",
                "gen": 0,
                "support": "hyperlinks",
                "metrics": self.metrics
            })

    def _accept(self, event: dict):
        """Generate an update event in response to the given input event.

        On the init event, a buffer window with id 0 is created.

        Line and char events are always accepted, even if the app is not
        awaiting them. (This is to simplify the app.) It is assumed they are
        meant for the window 0.

        Line input "error" will cause a fatal error.

        Line input "char" will make the app wait for a char.

        Line input "multipleinputs" will make the app await a line input and a
        char input at the same time.

        Line input "closeall" will close all the windows.

        Line input "hyperlinkonly" will make the app wait for a single
        hyperlink with no other inputs.

        Line input "noinputs" will make the app await no inputs.

        Line input "opengrid" will make the app open a grid window.

        Line input "closegrid" will make the app close the grid window.

        Line input "updatewindow" will make the app update the window 0's metrics; it's left attribute will be incremented.

        Other line inputs will only be echoed in the window 0.

        Char inputs will make the app print "char " followed by the char
        pressed.

        Hyperlink inputs will make the app print "hyperlink " followed by the
        value of the hyperlink clicked.
        """
        event.setdefault("gen", -1)
        event.setdefault("type", "")
        # The generation does not match.
        if event["gen"] != self.generation:
            return glk.error_event(
                "The generation of the accepted event does not match the generation of the app."
            )

        new_gen = event["gen"] + 1

        # Init event.
        if event["type"] == "init":
            if self.init:
                return glk.error_event("The app has already been initialised.")
            self.init = True
            self.metrics = event["metrics"]
            return {
                "type": "update",
                "gen": new_gen,
                "windows": [{
                    "id": 0,
                    "type": "buffer",
                    "rock": 0,
                    "left": 0,
                    "top": 0,
                    "width": 10,
                    "height": 5
                }],
                "input": [line_input(new_gen)]
            }

        # Line event.
        if event["type"] == "line":
            # We'll cheat and always accept even if the id is not right.
            # (We'll always assume the input is meant for the window 0.)
            # The `accept` and `build_input` methods of `BaseApp` will make
            # some checks anyway.
            command = event["value"]
            if command == "error":
                return glk.error_event("Fatal error.")
            elif command == "multipleinputs":
                return {
                    "type": "update",
                    "gen": new_gen,
                    "input": [
                        line_input(new_gen),
                        char_input(new_gen),
                    ]
                }
            elif command == "char":
                return {
                    "type": "update",
                    "gen": new_gen,
                    "input": [char_input(new_gen)]
                }
            elif command == "closeall":
                return {
                    "type": "update",
                    "gen": new_gen,
                    "windows": {},
                    "input": [line_input(new_gen)]
                }
            elif command == "hyperlinkonly":
                update = {
                    "type": "update",
                    "gen": new_gen,
                    "content": [{
                        "id": 0,
                        "text": [
                            {"content": [{
                                "style": "normal",
                                "hyperlink": self.hyperlink_id,
                                "text": "link"
                            }]}
                        ]
                    }],
                    "input": [{"id": 0, "hyperlink": True}]
                }
                self.hyperlink_id += 1
                return update
            elif command == "noinputs":
                return {
                    "type": "update",
                    "gen": new_gen,
                    "input": []
                }
            elif command == "opengrid":
                return {
                    "type": "update",
                    "gen": new_gen,
                    "windows": [
                        {
                            "id": 0,
                            "type": "buffer",
                            "rock": 0,
                            "left": 0,
                            "top": 0,
                            "width": 10,
                            "height": 5
                        },
                        {
                            "id": 1,
                            "type": "grid",
                            "rock": 1,
                            "left": 0,
                            "top": 0,
                            "width": 10,
                            "height": 5,
                            "gridwidth": 80,
                            "gridheight": 1
                        }
                    ],
                    "input": [line_input(new_gen)]
                }
            elif command == "closegrid":
                return {
                    "type": "update",
                    "gen": new_gen,
                    "windows": [
                        {
                            "id": 0,
                            "type": "buffer",
                            "rock": 0,
                            "left": 0,
                            "top": 0,
                            "width": 10,
                            "height": 5
                        }
                    ],
                    "input": [line_input(new_gen)]
                }
            elif command == "updatewindow":
                self.window_left += 1
                return {
                    "type": "update",
                    "gen": new_gen,
                    "windows": [
                        {
                            "id": 0,
                            "type": "buffer",
                            "rock": 0,
                            "left": self.window_left,
                            "top": 0,
                            "width": 10,
                            "height": 5
                        }
                    ],
                    "input": [line_input(new_gen)]
                }
            else:
                return {
                    "type": "update",
                    "gen": new_gen,
                    "content": [{
                        "id": 0,
                        "text": [
                            {"content": [{"style": "normal", "text": command}]}
                        ]
                    }],
                    "input": [line_input(new_gen)]
                }
        elif event["type"] == "char":
            char = event["value"]
            return {
                "type": "update",
                "gen": new_gen,
                "content": [{
                    "id": 0,
                    "text": [
                        {"content": [
                            {"style": "normal", "text": f"char {char}"}
                        ]}
                    ]
                }],
                "input": [line_input(new_gen)]
            }
        elif event["type"] == "hyperlink":
            return {
                "type": "update",
                "gen": new_gen,
                "content": [{
                    "id": 0,
                    "text": [
                        {"content": [
                            {"style": "normal", "text": f"hyperlink {event['value']}"}
                        ]}
                    ]
                }],
                "input": [line_input(new_gen)]
            }

        # Return an empty dict by default.
        return {}
