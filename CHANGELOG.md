# Changelog

## 2.0.0

This is a complete rewrite of the bot. It is now split in modules, is more easily extendable and has more features.

### Added

- The bot can be configured through a JSON file.
- The bot can be launched in dev mode, which will make it use a different configuration file.
- A command line interface allows to run the bot and simulate it, in dev or regular mode.
- The bot logs various events and errors.
- The `refresh` command returns specific messages if it has failed, stating the reason, instead of just saying all went well.
- The bot supports hyperlinks in stories; the `link` meta command is used to click them. Link styling can also be disabled or enabled with `!>>link off` and `!>>link on` (it is on by default).
- The bot supports stories with more windows than just a status bar and a buffer window. The content of each window is shown one after the other, separated by `---`.
- The bot can be localised, and a French translation has been added.
- The program is much more covered by tests.

### Changed

- Poetry is used for dependencies.
- The last versions of dependencies are used.
- The Discord token for the bot is stored in `secrets.json` instead of `secrets.txt`.
- The default values of the meta prompt and the story prompt are `!>>` and `!>` instead of `>>` and `>`, because Discord now supports quotes (messages starting with `>`).
- Apps (previously named Stories) now remember what content has not been shown yet, and show it all when asked. Before, they would only output what happened since the last command.
- Line breaks are replaced by spaces in Discord messages sent to the bot.
- The case of the story file extensions is now ignored when choosing an interpreter.
- Specific messages are returned when submitting a story command to the bot for edge cases (the app is stopped, accepts no inputs or only accepts hyperlinks).
- Some type hints have been added to the code.

### Fixed

- Shell commands executed to launch the interpreter processes are now properly escaped, which leads to less risk.
- There are no longer errors if an intepreter could not be launched (because it's missing or it lacks the permissions, for instance).
- If a bot's message is too long for Discord and is split into multiple messages, styles are correctly closed and reopened if the splitting happened in the middle of one.
- The Discord playing status of the bot is always accurate. Before, it showed only the last app launched, and showed nothing when the last app launched was stopped, even if others were still playing.
- Apps now stop correctly as soon as they no longer accept input. Previously, they were stopped only when their output were retrieved.
- Inputs fields from RemGlk are now handled properly. (When they are missing, it means they are unchanged, not that they are cancelled.)
- Apps are terminated with an error if their `_accept` method returns a RemGlk error event.

## 1.0.0 (2019-09-17)

- Initial release.

There never were a proper release for the initial version, so the point just before the development of the version 2.0.0 is retroactively called 1.0.0.
