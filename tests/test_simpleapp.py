from xyzzanie import glk

from .simpleapp import SimpleApp


class TestSimpleApp:
    """Test for the `SimpleApp` class defined in `tests.simpleapp`.

    The `SimpleApp` class is only used for tests, but we must make sure it
    behaves as intended so that the tests for `BaseApp` work correctly.
    """

    def test_accept_with_wrong_gen(self):
        app = SimpleApp(autoinit=False)
        result = app._accept({
            "type": "init",
            "gen": 1,
            "support": "hyperlinks",
            "metrics": app.metrics
        })
        assert result["type"] == "error"

    def test_accept_init(self):
        app = SimpleApp(autoinit=False)
        result = app._accept({
            "type": "init",
            "gen": 0,
            "support": "hyperlinks",
            "metrics": {"height": 10, "width": 5}
        })
        assert app.metrics == {"height": 10, "width": 5}
        assert result == {
            "type": "update",
            "gen": 1,
            "windows": [{
                "id": 0,
                "type": "buffer",
                "rock": 0,
                "left": 0,
                "top": 0,
                "width": 10,
                "height": 5
            }],
            "input": [{
                "id": 0,
                "gen": 1,
                "type": "line",
                "maxlen": 100
            }]
        }

    def test_accept_init_twice(self):
        app = SimpleApp(autoinit=False)
        app._accept({
            "type": "init",
            "gen": 0,
            "support": "hyperlinks",
            "metrics": {"height": 10, "width": 5}
        })
        result = app._accept({
            "type": "init",
            "gen": 1,
            "support": "hyperlinks",
            "metrics": {"height": 10, "width": 5}
        })
        assert result["type"] == "error"

    def test_accept_line_error(self):
        app = SimpleApp()
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "error"
        })
        assert result == glk.error_event("Fatal error.")

    def test_accept_lie_char(self):
        app = SimpleApp()
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "char"
        })
        assert len(result["input"]) == 1
        assert result["input"][0]["type"] == "char"

    def test_accept_line_multipleinputs(self):
        app = SimpleApp()
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "multipleinputs"
        })
        assert len(result["input"]) == 2
        assert result["input"][0]["type"] == "line"
        assert result["input"][1]["type"] == "char"

    def test_accept_line_closeall(self):
        app = SimpleApp()
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "closeall"
        })
        assert len(result["windows"]) == 0

    def test_accept_line_hyperlinkonly(self):
        app = SimpleApp()
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "hyperlinkonly"
        })
        assert len(result["input"]) == 1
        assert result["input"][0]["hyperlink"] == True  # noqa: E712
        assert "type" not in result["input"][0]

    def test_accept_noinputs(self):
        app = SimpleApp()
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "noinputs"
        })
        assert len(result["input"]) == 0

    def test_accept_opengrid(self):
        app = SimpleApp()
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "opengrid"
        })
        assert len(result["windows"]) == 2

    def test_accept_closegrid(self):
        app = SimpleApp()
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "closegrid"
        })
        assert len(result["windows"]) == 1

    def test_accept_updatewindow(self):
        app = SimpleApp()
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "updatewindow"
        })
        assert result["windows"][0]["left"] == 1
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "updatewindow"
        })
        assert result["windows"][0]["left"] == 2

    def test_accept_line_other(self):
        app = SimpleApp()
        result = app._accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "xyzzy"
        })
        assert result["content"] == [{
            "id": 0,
            "text": [
                {"content": [{"style": "normal", "text": "xyzzy"}]}
            ]
        }]

    def test_accept_unknown(self):
        app = SimpleApp()
        result = app._accept({
            "type": "",
            "gen": 1,
        })
        assert result == {}
