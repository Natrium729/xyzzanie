import logging
from string import Template

from xyzzanie import config
from .languages import languages

logger = logging.getLogger("xyzzanie.localisation.core")

default_language = "en"


def default_message(key=None):
    """Returns the default string when a key doesn't exist."""
    return f'ERROR: No message with key "{key}".'


def t(key, lang=None, args={}):
    """Return the message corresponding to `key` in the specified language.

    If `lang` is not given, the language in the config file is used.

    If the key doesn't exist in the given language, the default language is
    used.
    If the key still doesn't exist, a default message is returned.

    `args` is a dict containing arguments for the Template string returned.
    """

    default_args = {
        "meta_prompt": config.meta_prompt,
        "story_prompt": config.story_prompt
    }
    default_args.update(args)
    args = default_args

    if lang is None:
        lang = config.language

    if lang not in languages:
        logger.warning(
            f'The "{lang}" translation does not exist. Falling back to "{default_language}".'
        )
        lang = default_language

    if key not in languages[lang]:
        if lang != default_language:
            logger.warning(
                f'The key "{key}" is missing from the language "{lang}". Falling back language "{default_language}".'
            )
            lang = default_language
            if key not in languages[lang]:
                logger.warning(
                    f'The key "{key}" is missing from the default language "{default_language}".'
                )
                return default_message(key)
        logger.warning(
            f'The key "{key}" is missing from the language "{lang}".'
        )
        return default_message(key)

    # TODO: Cache template objects instead of instancing a new one each time?
    return Template(languages[lang][key]).safe_substitute(args)
