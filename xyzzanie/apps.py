from enum import Enum
import json
import logging
import os
import platform
import shlex
from typing import Union, Iterable

import pexpect
from pexpect import popen_spawn

from xyzzanie import config
from xyzzanie import formatting
from xyzzanie import glk
from xyzzanie.localisation import t


logger = logging.getLogger("xyzzanie.apps")


def join_command(split_command: Iterable[str]):
    """Return a shell-escaped string from `split_command`."""
    return " ".join(shlex.quote(arg) for arg in split_command)


# This function is a copy of `subprocess.list2cmdline`. We don't import it
# directly because it is considered an internal implementation detail.
# (See https://github.com/python/cpython/blob/3.8/Lib/subprocess.py)
def join_command_windows(seq: Iterable[str]):
    """Join a list of arguments into a Windows command line."""

    result = []
    needquote = False
    for arg in map(os.fsdecode, seq):
        bs_buf = []

        # Add a space to separate this argument from the others
        if result:
            result.append(" ")

        needquote = (" " in arg) or ("\t" in arg) or not arg
        if needquote:
            result.append('"')

        for c in arg:
            if c == "\\":
                # Don't know if we need to double yet.
                bs_buf.append(c)
            elif c == '"':
                # Double backslashes.
                result.append("\\" * len(bs_buf) * 2)
                bs_buf = []
                result.append('\\"')
            else:
                # Normal char
                if bs_buf:
                    result.extend(bs_buf)
                    bs_buf = []
                result.append(c)

        # Add remaining backslashes, if any.
        if bs_buf:
            result.extend(bs_buf)

        if needquote:
            result.extend(bs_buf)
            result.append('"')

    return "".join(result)


# This function is used for the filenames of the stories. (This is why spaces
# are forbidden: since user's commands are split at spaces, it would be
# impossible to refer to a story with spaces in its name.)
# I don't think it catches every problem (or even if it is helpful in the end),
# but I guess it can't hurt. I could maybe restrict filenames to letters and
# numbers only?
def filename_is_safe(name):
    """Returns whether or not a filename is safe.

    A filename is considered safe if it doesn't contain spaces, or specific
    punctuation characters.
    """
    return not (" " in name or ":" in name or "\\" in name or ";" in name or "'" in name or '"' in name)


def loads_multiple_object_json(s):
    """Converts a string containing multiple JSON objects at the root to a
    list of dictionaries."""

    decoder = json.JSONDecoder()
    found_objects = []
    buffer = s.strip()
    i = 0
    while buffer:
        try:
            result = decoder.raw_decode(buffer)
            found_objects.append(result[0])
            buffer = buffer[result[1]:].strip()
        except Exception as e:
            print(e)
        i += 1
        if i > 10000:
            break
    return found_objects


class BuildInputResult(Enum):
    """Possible return values of `BaseApp.build_input`."""
    STORY_STOPPED = "story_stopped"
    NO_INPUTS = "no_inputs"
    ONLY_HYPERLINKS = "only_hyperlinks"
    SUCCESS = "success"


class HyperlinkResult(Enum):
    """Possible return values of `BaseApp.click_hyperlink`"""
    STORY_STOPPED = "story_stopped"
    NO_INPUT = "no_input"
    SUCCESS = "success"
    INVALID_VALUE = "invalid_value"


class RefreshStoriesResult(Enum):
    """Possible return values of `InterpreterApp.refresh_stories_available`."""
    SUCCESS = "success"
    FOLDER_MISSING = "folder_missing"
    ERROR = "error"


class BaseApp:
    """Base class for Glk apps."""

    def __repr__(self):
        return f"apps.BaseApp(debug={self.debug!r})"

    def __init__(self, debug=False):
        self.debug = debug
        self.stopped = False

        self.generation = 0
        self.windows = {}  # For the opened windows. Keys will be ints!
        self.inputs = []
        self.metrics = {"width": 80, "height": 24}  # Size of the viewport.
        self.last_error = {}  # The last error encountered by the app.

        # The last Remglk update of the app as a dict. Only used for debugging.
        self.last_update = {}

    def terminate(self):
        """Stop the app.

        The attribute `stopped` will be set to `True`.
        """

        self.stopped = True

    def error(self, msg):
        """Terminate the app with an error.

        The attribute `self.last_error` will be set to a Glk error with the
        message given.
        """

        self.last_error = glk.error_event(msg)
        self.terminate()

    def build_input(self, value):
        """Create a Glk input event from a command and feed it to the app.

        Returns `BuildInputResult.STORY_STOPPED` if `self.stopped` is `True`,
        `BuildInputResult.NO_INPUTS` if the story is not awaiting any inputs,
        `BuildInputResult.ONLY_HYPERLINKS` if it is only awaiting hyperlinks.
        In those cases, the method does nothing else.

        Returns `BuildInputResult.SUCCESS` if everything went right and an
        input (char or line) has been sent to the app.
        """

        # The story is stopped.
        if self.stopped:
            logger.warning(
                f"Cannot build input for {self} since it is stopped."
            )
            return BuildInputResult.STORY_STOPPED

        # The story is not awaiting any inputs.
        if not self.inputs:
            logger.warning(
                f"Cannot build input for {self} because it has no inputs."
            )
            return BuildInputResult.NO_INPUTS

        # We take the first line or char input. (There should be only one at
        # this stage.)
        # There may be more inputs for hyperlinks, hence the loop.
        pending_input = None
        for input_ in self.inputs:
            if "type" in input_:
                pending_input = input_
                break
        # There are only hyperlink inputs.
        if pending_input is None:
            logger.warning(
                f"Cannot build input for {self} because it is only awaiting hyperlinks."
            )
            return BuildInputResult.ONLY_HYPERLINKS

        # If the command is empty, we replace it with a space, so that there
        # will be a character if the input type is "char".
        if not value:
            value = " "

        input_event = {"gen": self.generation, "window": pending_input["id"]}

        # The story is waiting for a command.
        if pending_input["type"] == "line":
            input_event["type"] = "line"
            input_event["value"] = value[:pending_input["maxlen"]]

        # The story is waiting for a keypress.
        elif pending_input["type"] == "char":
            input_event["type"] = "char"
            if value in glk.special_keys:
                input_event["value"] = value
            else:
                input_event["value"] = value[0]  # The first character only.

        self.accept(input_event)
        return BuildInputResult.SUCCESS

    def click_hyperlink(self, value) -> HyperlinkResult:
        """Find an hyperlink corresponding to `value` in the app and click it.

        Returns `HyperlinkResult.NO_INPUT` if th app isn't awaiting inputs,
        `HyperlinkResult.SUCCESS` if a link was found and clicked, and
        `HyperlinkResult.INVALID_VALUE` if no hyperlink with the given value
        has been found.
        """

        # The story is stopped.
        if self.stopped:
            logger.warning(
                f"Cannot click hyperlink in {self} since it is stopped."
            )
            return HyperlinkResult.STORY_STOPPED

        if not self.inputs:
            logger.warning(
                f"Cannot click hyperlink in {self} because it has no inputs."
            )
            return HyperlinkResult.NO_INPUT

        for input_ in self.inputs:
            if (
                input_.get("hyperlink", False)
                and value in self.windows[input_["id"]].hyperlinks
            ):
                input_event = {
                    "type": "hyperlink",
                    "gen": self.generation,
                    "window": input_["id"],
                    "value": value
                }
                self.accept(input_event)
                return HyperlinkResult.SUCCESS

        return HyperlinkResult.INVALID_VALUE

    def accept(self, event):
        """Input a glk event to the story.

        This method will increment the generation, call `self._accept` to get
        the next update event, and update the state of the Glk app. The
        `_accept` method should never be called directly.
        """

        # Don't accept event if the story has stopped.
        if self.stopped:
            return

        # Get the next update event.
        update = self._accept(event)
        # TODO: Check if the generation of the update has the right value?
        update.setdefault("type", "error")  # Type defaults to an error.

        if update["type"] == "error":
            error_message = update.get("message", "Error without message.")
            self.error(error_message)
            return

        elif update["type"] == "update":
            if "input" in update:
                self.inputs = update["input"]
                # No inputs means the story has ended or quit.
                if not self.inputs:
                    self.terminate()
                input_count = 0
                for input_ in self.inputs:
                    # It's a line or char input.
                    if "type" in input_:
                        input_count += 1
                # If there are more than one line/char input.
                if input_count > 1:
                    self.error(t("story_with_several_inputs"))
                    return

            if "windows" in update:
                windows_update = update["windows"]

                # All windows has been closed. Should not happen very often.
                if not windows_update:
                    self.error(t("story_all_windows_closed"))
                    return

                old_windows = self.windows
                self.windows = {}
                for window in windows_update:
                    id_ = window["id"]
                    # It's a window being updated.
                    if id_ in old_windows:
                        self.windows[id_] = old_windows[id_]
                        glk.update_window(self.windows[id_], window)
                    # It's a new window.
                    else:
                        self.windows[id_] = glk.create_window(window)

            # Update the content of the windows.
            for content in update.get("content", []):
                content_id = content["id"]
                current_window = self.windows.get(content_id, None)
                if isinstance(current_window, glk.BufferWindow):
                    current_window.queue_content(
                        content.get("text", []),
                        clear=content.get("clear", False)
                    )
                elif isinstance(current_window, glk.GridWindow):
                    current_window.update_content(content.get("lines", []))
                # GRPHCS
                # elif isinstance(current_window, glk.GraphicsWindow):
                    # current_window.update_content(content)

        # Bump the generation.
        self.generation += 1

        # TODO: Check if the inputs in the update has the current generation?
        self.last_update = update

    def _accept(self, event) -> dict:
        """Generate a Glk update event in response of the `event` input.

        This method is never called directly and is instead called by `accept`.

        Raises `NotImplementedError` by default. This method should be
        overridden in a subclass to have a useful effect.
        """

        raise NotImplementedError

    def output(self) -> Union[str, glk.Display]:
        """Return the windows' new content since this method was last called,
        as a `glk.Display` instance.

        If `self.last_error` is not empty, then the error message, formatted,
        will be returned instead.

        If the story is stopped, the new content will be returned as usual
        along with a message saying that the story has ended; this message
        will take the form of an additionnal buffer window in the
        `glk.Display` returned. The `self.last_error` attribute will also be
        set to an error with this message, so that calling this method again
        will only return the message.

        If `self.debug` is `True`, the dictionary representing the last glk update event output by the app will be returned instead.
        """

        # TODO: This method can return a string OR a glk.Display.
        # (Or a dict if self.debug is True, but that's less important.)
        # Is this a problem or should we make it always return a glk.Display?

        # Only return the error message if there is one.
        # New content that happened before the error will never be shown, but
        # that's OK: the player won't be able to continue playing anyway.
        if self.last_error:
            return formatting.to_markdown(
                self.last_error["message"],
                formatting.Style.X_ERROR
            )

        grid_outputs = []
        buffer_outputs = []
        # GRPHCS
        # graphics_output = []

        for window in self.windows.values():
            if isinstance(window, glk.GridWindow):
                grid_outputs.append(window.get_content())
            elif isinstance(window, glk.BufferWindow):
                buffer_outputs.append(window.get_pending_content())
            # GRPHCS
            # elif isinstance(window, glk.GraphicsWindow):
                # pass

        if self.stopped:
            message = t("story_ended", args={"story": self.story})
            buffer_outputs.append([formatting.Text(message, formatting.Style.X_INFO)])
            self.error(message)

        # Output the raw event if the story is in debug mode.
        if self.debug:
            return self.last_update

        # GRPHCS: Add graphics window argument.
        return glk.Display(grids=grid_outputs, buffers=buffer_outputs)


class InterpreterApp(BaseApp):
    """App that launch an interpreter process for the given story file."""

    # For security, only files who are in this list (populated with the
    # `refresh_stories_available` method below) can be played, since they
    # have been checked first.
    # If we allowed any filename, there could be a risk that a player could
    # interact with the computer hosting the bot.
    stories_available = []

    @classmethod
    def refresh_stories_available(cls):
        """Look into the story folder to populate `stories_available` with
        files that have a safe name.

        Returns `RefreshStoriesResult.SUCCESS` if everything went well,
        `RefreshStoriesResult.FOLDER_MISSING` if the story folder doesn't
        exist and `RefreshStoriesResult.ERROR` if an `OSError` was raised.
        """

        cls.stories_available = []
        try:
            is_dir = config.stories_path.is_dir()
        except OSError:
            logger.exception(
                f"Cannot read the folder at story folder at <{config.stories_path}>.",
            )
            return RefreshStoriesResult.ERROR
        if is_dir:
            for entry in config.stories_path.iterdir():
                name = entry.name
                if not name.startswith(".") and entry.is_file() and entry.suffix in config.terps:
                    if not filename_is_safe(name):
                        logger.warning(
                            f'The filename of a story cannot contain the characters "/", "\\", ";" and "\'", quotation marks or spaces. Please rename "{name.replace(":", "/")}".'
                        )
                    else:
                        cls.stories_available.append(entry.name)
            return RefreshStoriesResult.SUCCESS
        else:
            logger.error(f"The stories folder at <{config.stories_path}> is missing.")
            return RefreshStoriesResult.FOLDER_MISSING

    def __str__(self):
        return str(self.story)

    def __init__(self, story, terp=None, debug=False):
        """The constructor for a new Story.

        The `story` argument is the name of the story file, and `terp` is the
        name of the interpreter used to read the story.

        If no terp is given, one will be chosen automatically according to
        `config.terps`.

        If the interpreter cannot be launched (for example if the interpreter
        does not exist), the app is immediately terminated.
        """

        super().__init__(debug=debug)

        self.story = story

        # TODO: Move the choice of the terp out of the class so that the class
        # is not coupled with the rest of the bot (the config file)?
        if terp is None:
            extension = os.path.splitext(story)[-1].lower()
            self.terp = config.terps.get(extension, None)
            if self.terp is None:
                logger.error(
                    f'No interpreter has been specified in the config file for the extension "{extension}".'
                )
                self.terminate()
                return

        try:
            if platform.system() == "Windows":
                command = join_command_windows([
                    str(config.interpreters_path / self.terp),
                    str(config.stories_path / self.story)
                ])
                self.process = popen_spawn.PopenSpawn(
                    command,
                    encoding="UTF-8"
                )
            else:
                command = join_command([
                    str(config.interpreters_path / self.terp),
                    str(config.stories_path / self.story)
                ])
                self.process = pexpect.spawn(
                    command,
                    encoding="UTF-8"
                )
        except (OSError, pexpect.ExceptionPexpect):
            logger.exception(f"The interpreter {self.terp} could no be launched.")
            self.terminate()
            return

        # Give the story its first input.
        self.accept({
            "type": "init",
            "gen": 0,
            "support": ["hyperlinks"],
            "metrics": self.metrics
        })

    def terminate(self):
        """Stop the app.

        The attribute `stopped` will be set to `True` and the interpreter
        process will be terminated.
        """

        super().terminate()

        if hasattr(self, "process"):
            if platform.system() == "Windows":
                # TODO : Find a way to terminate a process in Windows.
                pass
            else:
                self.process.terminate(force=True)

    def _accept(self, event):
        """Generates the next update based on the given event.

        Inputs the given event to the interpreter process and retrieves it
        output.
        If the output contains some unsupported features (multiple windows or
        inputs), the method will return an error update instead.
        """

        self.process.sendline(json.dumps(event))
        # RemGlk outputs seem to always end with these linebreaks.
        self.process.expect_exact(["\r\n\r\n", "\n\n", pexpect.EOF, pexpect.TIMEOUT], timeout=10)
        output = loads_multiple_object_json(self.process.before)[-1]
        return output
