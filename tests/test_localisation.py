from xyzzanie import config
import xyzzanie.localisation as l10n
from xyzzanie.localisation import t


testing_language = {
    "lipsum": "Lorem ipsum dolor sit amet.",
    "lipsum_with_args": "Lorem ipsum $alpha sit $beta.",
    "lipsum_with_prompts": "${meta_prompt}Lorem ${story_prompt}ipsum."
}

l10n.languages.languages["test"] = testing_language


class TestT:
    """Tests for the function `t`."""

    def test_no_language_supplied(self):
        """Current language is used when no language is given."""
        key = "no_reply_returned"
        message = t(key)
        assert message == l10n.languages.en[key]

    def test_language_nonexistent(self):
        """English is used if the language given is nonexistent."""
        key = "no_reply_returned"
        message = t(key, lang="nonexistent")
        assert message == l10n.languages.en[key]

    def test_key_nonexistent_in_given_language(self):
        """English is used as a fallback when a key doesn't exist."""
        key = "no_reply_returned"
        message = t(key, lang="testing_language")
        assert message == l10n.languages.en[key]

    def test_nonexistent_in_given_language_and_default(self):
        """A default message is returned."""
        key = "NONEXISTENT"
        message = t("NONEXISTENT", lang="test")
        assert message == l10n.core.default_message(key)

    def test_nonexistent_in_default(self):
        """A default message is returned."""
        key = "NONEXISTENT"
        message = t("NONEXISTENT", lang=l10n.default_language)
        assert message == l10n.core.default_message(key)

    def test_language_given(self):
        message = t("lipsum", lang="test")
        assert message == "Lorem ipsum dolor sit amet."

    def test_with_args(self):
        args = {"alpha": "Hello", "beta": "World"}
        message = t("lipsum_with_args", lang="test", args=args)
        assert message == "Lorem ipsum Hello sit World."

    def test_default_args(self):
        message = t("lipsum_with_prompts", lang="test")
        assert message == f"{config.meta_prompt}Lorem {config.story_prompt}ipsum."
