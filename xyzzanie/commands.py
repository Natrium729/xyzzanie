from inspect import isfunction
import logging

from xyzzanie import apps
from xyzzanie import config
from xyzzanie import formatting
from xyzzanie.localisation import t

logger = logging.getLogger("xyzzanie.commands")


class Reply:
    """A reply to a user's command.

    The message of the reply is stored in the `message` attribute of a reply,
    and an identification string is stored in its `id` attribute."""

    # The following class attributes are the identifiers that can be used when
    # creating a reply.

    META_EMPTY = "meta_empty"
    META_UNKNOWN = "meta_unknown"
    META_NO_REPLY = "meta_no_reply"

    STORY_NOT_PLAYING = "story_not_playing"
    STORY_STOPPED = "story_stopped"
    STORY_NO_INPUTS = "story_no_inputs"
    STORY_ONLY_HYPERLINKS = "story_only_hyperlinks"
    STORY_ENDED = "story_ended"
    STORY_OUTPUT = "story_output"

    PARSE_NOT_A_COMMAND = "parse_not_a_command"

    HELP = "help"

    ABOUT = "about"

    REFRESH_OK = "refresh_ok"
    REFRESH_MISSING = "refresh_missing"
    REFRESH_ERROR = "refresh_error"

    LIST = "list"

    PLAY_ALREADY_PLAYING = "play_already_playing"
    PLAY_NO_STORY_GIVEN = "play_no_story_given"
    PLAY_NOT_IN_LIST = "play_not_in_list"
    PLAY_CANNOT_OPEN = "play_cannot_open"
    PLAY_APP_CANNOT_BE_LAUNCHED = "play_app_cannot_be_launched"
    PLAY_STARTED = "play_started"
    PLAY_NONEXISTENT_FILE = "play_nonexistent_file"

    LINK_NO_ARGS = "link_no_args"
    LINK_INVALID_ARG = "link_invalid_arg"
    LINK_ENABLE = "link_enable"
    LINK_DISABLE = "link_disable"
    LINK_NO_INPUTS = "link_no_inputs"
    LINK_INVALID_VALUE = "link_invalid_value"
    LINK_CLICKED = "link_clicked"

    FORCEQUIT = "forcequit"
    FORCEQUIT_WITHOUT_STORY = "forcequit_without_story"

    XYZZY = "xyzzy"

    def __init__(self, message, identifier):
        self.message = message
        self.id = identifier

    def __repr__(self):
        return f"commands.Reply({self.message!r}, {self.id!r})"

    def __str__(self):
        return str(self.message)


running_apps = {}


def app_running_in(location):
    return location in running_apps


def add_app_to_location(app, location):
    """Add the app to the specified location."""
    if app_running_in(location):
        logger.error(f'Tried to add {app} to "{location}", which is not free.')
        raise ValueError(f"An app is already running in {location}.")
    running_apps[location] = app


def get_app_in(location) -> apps.BaseApp:
    return running_apps.get(location, None)


def remove_app_from_location(location):
    """Remove the app from the specified location."""
    running_apps.pop(location, None)


def handle_meta_command(location, command: str) -> Reply:
    """Execute a meta commmand in `location` and return its reply.

    If the command is empty, a reply telling that is returned.

    If there is a function in `meta_commands` associated with the first
    word of the command, it is executed with the other words as arguments.

    If the command was executed but did not return a `Reply`, a reply
    indicating that is returned.

    If the command is not in `meta_commands`, an error message is returned.
    """

    arguments = command.split()

    # Empty meta command.
    if not arguments:
        message = formatting.to_markdown(
            t("meta_command_empty"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.META_EMPTY)

    action = arguments.pop(0)

    # Invalid meta command.
    if action not in meta_commands:
        message = formatting.to_markdown(
            t("meta_command_invalid", args={"command": action}),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.META_UNKNOWN)

    # Valid meta command.
    reply = meta_commands[action](location, *arguments)
    # The function returned a reply.
    if isinstance(reply, Reply):
        return reply
    else:
        logger.warning(
            "The meta command didn't return a Reply object. A reply with the received object as a message was sent instead."
        )
        if reply is None:
            reply = formatting.to_markdown(
                t("meta_command_no_reply"),
                formatting.Style.X_ERROR
            )
        return Reply(reply, Reply.META_NO_REPLY)


def handle_story_command(location, command: str) -> Reply:
    """Submit a story commmand to the app in `location` and return its reply.

    If no app is being running in the location, an error message is returned.
    """
    app = get_app_in(location)

    # No story is being played.
    if app is None:
        message = formatting.to_markdown(
            t("story_not_playing"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.STORY_NOT_PLAYING)

    result = app.build_input(command)
    if result == apps.BuildInputResult.STORY_STOPPED:
        return Reply(
            t("story_is_stopped"),
            Reply.STORY_STOPPED
        )
    if result == apps.BuildInputResult.NO_INPUTS:
        return Reply(
            t("story_no_inputs"),
            Reply.STORY_NO_INPUTS
        )
    if result == apps.BuildInputResult.ONLY_HYPERLINKS:
        return Reply(
            t("story_only_hyperlinks"),
            Reply.STORY_ONLY_HYPERLINKS
        )
    if result == apps.BuildInputResult.SUCCESS:
        message = app.output()
        if app.stopped:
            remove_app_from_location(location)
            # If the story is not playing after the command has been
            # parsed, we return a different identifier.
            return Reply(message, Reply.STORY_ENDED)
        return Reply(message, Reply.STORY_OUTPUT)


def parse_command(location, command: str) -> Reply:
    """Determine if the given command does something and handle it.

    This function returns a `Reply` containing the message displayed to the
    user and a string identifying what has happened.

    If the command starts with the meta prompt, passes it to
    `handle_meta_command` and return its result.

    If the command starts with the story prompt, pass it to
    `handle_story_command` and return its result.

    Finally, if the command does not start with a prompt (meta or story), an
    empty reply is returned, along with an id telling so.
    """

    # Replace line breaks in the command with spaces.
    command = " ".join(command.splitlines())

    # A meta command.
    if command.startswith(config.meta_prompt):
        return handle_meta_command(
            location,
            command[len(config.meta_prompt):]
        )
    # A story command.
    elif command.startswith(config.story_prompt):
        return handle_story_command(
            location,
            command[len(config.story_prompt):]
        )
    # The command is in fact not a command.
    else:
        return Reply("", Reply.PARSE_NOT_A_COMMAND)


# The following dictionary will map meta commands to functions and will be populated with the `register_meta` decorator.
meta_commands = {}


def register_meta(arg=None):
    """Register a function as a meta command for the bot.

    Without an argument, the command will be the name of the decorated
    function.

    If a string is given, then it will be the command associated with the
    decorated function.

    The arguments of the decorated functions should be the location (a string)
    followed by *args.
    """

    if isfunction(arg):
        meta_commands[arg.__name__] = arg
        return arg
    else:
        def decorator(func):
            meta_commands[arg] = func
            return func
        return decorator


# TODO: Make commands return a formatting.Text instead of a string?
# A Text would make the replies split correctly (but this should never happen
# since the messages are small). A string should be faster since it is not
# processed by the bot, but this should be negligible.


@register_meta("help")
def get_help(location, *args):
    """Return a help message and a link to the online help.

    If arguments are given, they are silently ignored.
    """

    message = formatting.to_markdown(t("help"), formatting.Style.X_INFO)
    return Reply(message, Reply.HELP)


@register_meta
def about(location, *args):
    """Return a message informing the user about the bot, such as its author.

    If arguments are given, they are silently ignored.
    """

    message = formatting.to_markdown(t("about"), formatting.Style.X_INFO)
    return Reply(message, Reply.ABOUT)


@register_meta
def refresh(location, *args):
    """Refresh the story list, in case stories were added are removed.

    If arguments are given, they are silently ignored.
    """

    result = apps.InterpreterApp.refresh_stories_available()
    if result is apps.RefreshStoriesResult.SUCCESS:
        message = formatting.to_markdown(
            t("story_list_refreshed"),
            formatting.Style.X_INFO
        )
        return Reply(message, Reply.REFRESH_OK)
    elif result is apps.RefreshStoriesResult.FOLDER_MISSING:
        message = formatting.to_markdown(
            t("story_folder_missing"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.REFRESH_MISSING)
    else:
        message = formatting.to_markdown(
            t("story_folder_unopenable"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.REFRESH_ERROR)


# The function can't be called `list` because it conflicts with the built-in
# type, so we give the command an explicit name.
@register_meta("list")
def list_stories(location, *args):
    """List the stories available.

    If arguments are given, they are silently ignored.
    """

    stories_available = "\n".join(apps.InterpreterApp.stories_available)
    message = formatting.Text(
        f"# {t('story_list_title')}\n\n{stories_available}",
        formatting.Style.X_MARKDOWN_BLOCK
    )
    return Reply(message, Reply.LIST)


@register_meta
def play(location: str, *args):
    """Try to load the story given in argument for the specified location.

    If a story is already being played, the story does not exist or no argument
    is given, an error message is returned.

    If the argument is valid (it is in `InterpreterApp.stories_available`)
    tries to start it.

    If more than one argument is given, the others are silently ignored.
    """

    app = get_app_in(location)
    if app is not None:
        message = formatting.to_markdown(
            t("story_already_playing", args={"story": app}),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.PLAY_ALREADY_PLAYING)
    if not args:
        message = formatting.to_markdown(
            t("no_story_given"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.PLAY_NO_STORY_GIVEN)

    story_filename = args[0]
    story_path = config.stories_path / story_filename
    if story_filename not in apps.InterpreterApp.stories_available:
        message = formatting.to_markdown(
            t("story_not_in_list", args={"filename": story_filename}),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.PLAY_NOT_IN_LIST)
    try:
        is_file = story_path.is_file()
    except OSError:
        logger.exception(f"Cannot read the file at <{story_path}>.")
        message = formatting.to_markdown(
            t("story_unopenable", args={"filename": story_filename}),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.PLAY_CANNOT_OPEN)
    if is_file:
        app = apps.InterpreterApp(story_filename)
        if app.stopped:
            message = formatting.to_markdown(
                t("interpreter_unlaunchable"),
                formatting.Style.X_ERROR
            )
            return Reply(message, Reply.PLAY_APP_CANNOT_BE_LAUNCHED)
        add_app_to_location(app, location)
        return Reply(app.output(), Reply.PLAY_STARTED)
    else:
        logger.warning(f"Cannot read the file at <{story_path}>.")
        message = formatting.to_markdown(
            t("story_file_nonexistent", args={"filename": story_filename}),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.PLAY_NONEXISTENT_FILE)


# TODO: For the moment, this setting is bot wide. Should it be specific to the
# app running, or to the channel? (A problem if it's app-specific is that
# it'll be reset when launching a new app.)
# Also, I don't the like the fact it's a global variable.
show_hyperlinks = True


@register_meta
def link(location: str, *args):
    """Click on an hyperlink in the app running in the given location.

    The first argument is the ID of the link.

    If more arguments are given, they are silently ignored.
    """

    global show_hyperlinks

    if not args:
        message = formatting.to_markdown(
            t("link_invalid_arg"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.LINK_NO_ARGS)

    arg = args[0].lower()

    if not arg.isdecimal() and arg not in ("on", "off"):
        message = formatting.to_markdown(
            t("link_invalid_arg"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.LINK_INVALID_ARG)

    # TODO: Show a specific message if the links are already enabled?
    # (The same when disabling.)
    if arg == "on":
        show_hyperlinks = True
        message = formatting.to_markdown(
            t("link_enable"),
            formatting.Style.X_INFO
        )
        return Reply(message, Reply.LINK_ENABLE)

    if arg == "off":
        show_hyperlinks = False
        message = formatting.to_markdown(
            t("link_disable"),
            formatting.Style.X_INFO
        )
        return Reply(message, Reply.LINK_DISABLE)

    app = get_app_in(location)
    # No story is being played.
    if app is None:
        message = formatting.to_markdown(
            t("story_not_playing"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.STORY_NOT_PLAYING)

    result = app.click_hyperlink(int(args[0]))
    if result == apps.HyperlinkResult.NO_INPUT:
        message = formatting.to_markdown(
            t("story_no_inputs"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.LINK_NO_INPUTS)
    elif result == apps.HyperlinkResult.INVALID_VALUE:
        message = formatting.to_markdown(
            t("link_invalid_value"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.LINK_INVALID_VALUE)
    elif result == apps.HyperlinkResult.SUCCESS:
        message = app.output()
        if app.stopped:
            remove_app_from_location(location)
            # If the story is not playing after the link has been clicked,
            # we return a different identifier.
            return Reply(message, Reply.STORY_ENDED)
        return Reply(message, Reply.STORY_OUTPUT)


@register_meta
def forcequit(location: str, *args):
    """Tries to quit the current story in the given location.

    If no story is currently being played, an error message is returned.

    If arguments are given, they are silently ignored.
    """
    app = get_app_in(location)

    if app is None:
        message = formatting.to_markdown(
            t("forcequit_without_story"),
            formatting.Style.X_ERROR
        )
        return Reply(message, Reply.FORCEQUIT_WITHOUT_STORY)

    app.terminate()
    remove_app_from_location(location)
    message = formatting.to_markdown(
        t("forcequit"),
        formatting.Style.X_INFO
    )
    return Reply(message, Reply.FORCEQUIT)


@register_meta
def xyzzy(location, *args):
    """A small easter egg.

    If arguments are given, they are silently ignored.
    """

    message = "```Une voix caverneuse dit : « L'imagination, le meilleur des moteurs de jeu ! »```"
    return Reply(message, Reply.XYZZY)
