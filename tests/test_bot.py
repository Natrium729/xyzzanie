from unittest import mock

from xyzzanie import bot
from xyzzanie import commands
from xyzzanie import formatting
from xyzzanie import glk


class TestProcessBotOutput:
    """Tests for the `process_bot_output` function."""

    @mock.patch("xyzzanie.formatting.split_message")
    def test_text(self, mocked_split_message):
        mocked_split_message.return_value = "mocked_return_value"
        message = formatting.Text("Hello World", formatting.Style.EMPHASIZED)
        result = bot.process_bot_output(message)
        mocked_split_message.assert_called_once_with([message], 1900, hyperlinks=True)
        assert result == "mocked_return_value"

    @mock.patch("xyzzanie.formatting.split_message")
    def test_list_of_texts(self, mocked_split_message):
        mocked_split_message.return_value = "mocked_return_value"
        message = [formatting.Text("Hello World", formatting.Style.EMPHASIZED)]
        result = bot.process_bot_output(message)
        mocked_split_message.assert_called_once_with(message, 1900, hyperlinks=True)
        assert result == "mocked_return_value"

    @mock.patch("xyzzanie.formatting.split_message")
    def test_list_of_texts_without_hyperlinks(self, mocked_split_message):
        mocked_split_message.return_value = "mocked_return_value"
        message = [formatting.Text("Hello World", formatting.Style.EMPHASIZED)]
        result = bot.process_bot_output(message, hyperlinks=False)
        mocked_split_message.assert_called_once_with(message, 1900, hyperlinks=False)
        assert result == "mocked_return_value"

    def test_display(self):
        message = glk.Display(
            grids=[""],
            buffers=[
                [formatting.Text("Hello World", formatting.Style.EMPHASIZED)]
            ]
        )
        message.to_split_message = mock.MagicMock()
        message.to_split_message.return_value = "mocked_return_value"
        result = bot.process_bot_output(message)
        message.to_split_message.assert_called_once_with(1900, hyperlinks=True)
        assert result == "mocked_return_value"

    def test_display_without_hyperlinks(self):
        message = glk.Display(
            grids=[""],
            buffers=[
                [formatting.Text("Hello World", formatting.Style.EMPHASIZED)]
            ]
        )
        message.to_split_message = mock.MagicMock()
        message.to_split_message.return_value = "mocked_return_value"
        result = bot.process_bot_output(message, hyperlinks=False)
        message.to_split_message.assert_called_once_with(1900, hyperlinks=False)
        assert result == "mocked_return_value"

    def test_string(self):
        message = "x" * 1901
        assert bot.process_bot_output(message) == ["x" * 1900, "x"]

    def test_other(self):
        message = 729
        assert bot.process_bot_output(message) == ["729"]


class TestGetPlayingStatus:
    """Tests for the `get_playing_status` function."""

    @mock.patch.dict(commands.running_apps, clear=True)
    def test_no_app(self):
        assert bot.get_playing_status() == ""

    @mock.patch.dict(commands.running_apps, {"loc": "app"}, clear=True)
    def test_one_app(self):
        assert bot.get_playing_status() == "app"

    @mock.patch.dict(commands.running_apps, {"loc": "app", "loc2": "app2"}, clear=True)
    @mock.patch("xyzzanie.bot.t")
    def test_two_apps(self, mocked_t):
        mocked_t.return_value = "mocked_return_value"
        result = bot.get_playing_status()
        mocked_t.assert_called_once_with("playing_status_two_apps", args={
            "last_app": "app2"
        })
        assert result == "mocked_return_value"

    @mock.patch.dict(commands.running_apps, {"loc": "app", "loc2": "app2", "loc3": "app3"}, clear=True)
    @mock.patch("xyzzanie.bot.t")
    def test_three_apps(self, mocked_t):
        mocked_t.return_value = "mocked_return_value"
        result = bot.get_playing_status()
        mocked_t.assert_called_once_with("playing_status_more_apps", args={
            "last_app": "app3",
            "other_number": 2
        })
        assert result == "mocked_return_value"
