import json
import logging
from pathlib import Path
import sys


logger = logging.getLogger("xyzzanie.config")

root_path = Path(__file__).parents[1]

DEFAULTS = {
    "dev_mode": False,
    "config_path": root_path / "config.json",
    "stories_path": root_path / "stories",
    "interpreters_path": root_path / "interpreters",
    "meta_prompt": "!>>",
    "story_prompt": "!>",
    "terps": {
        ".z5": "bocfel",
        ".z8": "bocfel",
        ".zblorb": "bocfel",
        ".ulx": "git",
        ".gblorb": "git",
    },
    "language": "en",
}

# Below are the variables holding the settings.
# When adding a new one, we should not forget to add it to the globals in the
# update function.

dev_mode = DEFAULTS["dev_mode"]
config_path = DEFAULTS["config_path"]
stories_path = DEFAULTS["stories_path"]
interpreters_path = DEFAULTS["interpreters_path"]
meta_prompt = DEFAULTS["meta_prompt"]
story_prompt = DEFAULTS["story_prompt"]
terps = DEFAULTS["terps"]
language = DEFAULTS["language"]


def update(dev=None):
    global dev_mode
    global config_path
    global stories_path
    global interpreters_path
    global meta_prompt
    global story_prompt
    global terps
    global language

    if dev is not None:
        dev_mode = dev

    if dev:
        config_path = root_path / "config-dev.json"
        interpreters_path = root_path / "interpreters-dev"
        stories_path = root_path / "stories-dev"
    else:
        config_path = root_path / "config.json"
        interpreters_path = root_path / "interpreters"
        stories_path = root_path / "stories"

    # Load configuration file.
    logger.info(f"Opening config file at <{config_path}>")
    custom_config = {}
    if config_path.exists():
        try:
            with config_path.open() as f:
                custom_config = json.load(f)
        except OSError:
            logger.exception(
                "Cannot open the config file. Using default values."
            )
        except json.JSONDecodeError:
            logger.exception(
                "The config file is malformed. Using default values."
            )
    else:
        logger.info(
            "No config file found. Using default values."
        )

    # The meta prompt.
    meta_prompt = str(
        custom_config.get("meta_prompt", DEFAULTS["meta_prompt"])
    )
    if not meta_prompt:
        meta_prompt = DEFAULTS["meta_prompt"]
        logger.warning(
            'The "meta_prompt" field is empty. Using default value.'
        )

    # The story prompt.
    story_prompt = str(
        custom_config.get("story_prompt", DEFAULTS["story_prompt"])
    )
    if not story_prompt:
        story_prompt = DEFAULTS["story_prompt"]
        logger.warning(
            'The "story_prompt" field is empty. Using default value.'
        )

    # Check that the story prompt doesn't start with the meta prompt.
    if story_prompt.startswith(meta_prompt):
        logger.critical(
            "The story prompt can't start with the meta prompt. Now exiting."
        )
        sys.exit(1)

    # The supported story formats and their corresponding interpreter.
    custom_terps = custom_config.get("terps", DEFAULTS["terps"].copy())
    if not isinstance(terps, dict):
        logger.warning(
            'The "terp" field is not a dictionary. Using default value.'
        )
        terps = {}
    terps = DEFAULTS["terps"].copy()
    terps.update(custom_terps)

    language = str(custom_config.get("language", DEFAULTS["language"]))

    logger.info("Config loaded.")
