"""Controlling the Discord bot."""

import collections.abc
import logging
import os
from pathlib import Path
from typing import List, Optional
import sys

import discord

from xyzzanie import commands
from xyzzanie import config
from xyzzanie import formatting
from xyzzanie import glk
from xyzzanie import secrets
from xyzzanie.localisation import t


logger = logging.getLogger("xyzzanie.bot")


# This function splits a reply in multiple chunks if it's too long, because a
# Discord message has a limit of 2000 characters. We use 1900 here just to be
# on the safe side.
# TODO: The reply can be split in the middle of a word, which is not
# desirable.
def process_bot_output(message, hyperlinks=True) -> List[str]:
    """Split `message` according to its type.

    If `hyperlinks` is `False`, hyperlinks won't be styled

    The length of each element in the returned list won't exceed 1900
    characters.

    If it's a `formatting.Text` or a list of `formatting.Text`s, split it using
    `formatting.split_message`.

    If it's a `glk.Display`, split it using its method `to_split_message`.

    If it's a string, split into strings of length 1900.
    """

    # If it's a formatting.Text, make it a list of Texts so that the next
    # condition applies.
    if isinstance(message, formatting.Text):
        message = [message]

    # It's a sequence of formatting.Text.
    if (
        isinstance(message, collections.abc.Sequence)
        and not isinstance(message, str)
        and all(isinstance(i, formatting.Text) for i in message)
    ):
        return formatting.split_message(message, 1900, hyperlinks=hyperlinks)

    # It's a glk.Display.
    if isinstance(message, glk.Display):
        return message.to_split_message(1900, hyperlinks=hyperlinks)

    # Else return message as a string split into a list.
    message = str(message)
    return [message[i:i + 1900] for i in range(0, len(message), 1900)]


def find_cover(story: str) -> Optional[Path]:
    """Returns the path of the given story's cover.

    The cover is a JPG or PNG file in the stories folder with the same name as
    the story (e.g. "story.jpg" for "story.z5").

    Returns `None` if no cover is found.
    """

    story = os.path.splitext(story)[0]

    path = config.stories_path / f"{story}.jpg"
    if path.is_file():
        return path

    path = config.stories_path / f"{story}.png"
    if path.is_file():
        return path

    return None


def get_playing_status() -> str:
    """Get the current Discord playing status.

    Returns an empty string is no apps are running.
    """

    apps = list(commands.running_apps.values())

    if not apps:
        return ""

    if len(apps) == 1:
        return str(apps[0])

    if len(apps) == 2:
        return t("playing_status_two_apps", args={"last_app": apps[-1]})

    return t("playing_status_more_apps", args={
        "last_app": apps[-1],
        "other_number": len(apps) - 1
    })


last_playing_status = ""


# Our client.
client = discord.Client()


@client.event
async def on_ready():
    """Prints the bot's name and ID when it is ready."""
    print("Logged in as {}#{}".format(client.user.name, client.user.id))


@client.event
async def on_message(message):
    """Make the bot react to messages so that users can interact with it."""

    # So that the bot does not react to itself.
    if message.author == client.user:
        return

    # This should identify each channel uniquely.
    location = f"{message.guild.id}#{message.channel.id}"
    # We process the message.
    reply = commands.parse_command(location, message.content)

    # In case commands.parse_command didn't return a Reply object.
    # Should never happen.
    if not isinstance(reply, commands.Reply):
        await message.channel.send(formatting.to_markdown(
            t("no_reply_returned"),
            formatting.Style.X_ERROR)
        )
        return

    # It was not intended to the bot.
    if reply.id == commands.Reply.PARSE_NOT_A_COMMAND:
        return

    split_reply = process_bot_output(reply.message, hyperlinks=commands.show_hyperlinks)

    # Listing stories in private message.
    if reply.id == commands.Reply.LIST:
        for chunk in split_reply:
            await message.author.send(chunk)
    # Other commands not in pivate message.
    else:
        # Update Discord playing status if it has changed.
        # TODO: I dislike the use of a global variable.
        new_status = get_playing_status()
        global last_playing_status
        if new_status != last_playing_status:
            last_playing_status = new_status
            game = discord.Game(name=new_status) if new_status else None
            await client.change_presence(activity=game)
        if reply.id == commands.Reply.PLAY_STARTED:
            app = commands.get_app_in(location)
            cover_image = find_cover(app.story)
            if cover_image is not None:
                await message.channel.send(file=discord.File(cover_image))

        for chunk in split_reply:
            await message.channel.send(chunk)


def run(dev=False):
    """Run the bot."""
    if dev:
        token = secrets.token_dev
    else:
        token = secrets.token
    if not token:
        logger.critical(
            "The token is empty and the bot cannot be run. No exiting."
        )
        sys.exit(1)
    client.run(token)


def simulate(dev=False):
    """Simulate the bot in a terminal."""

    # The name of the initial location.
    current_location = "sim"

    # The two following functions are additional meta commands available when
    # we are simulating the bot.

    @commands.register_meta("location")
    def change_location(location, *args):
        """Change the play location to the specified one."""
        if not args:
            return commands.Reply(
                "Please give a location name!",
                "location_nothing_given"
            )
        nonlocal current_location
        current_location = args[0]
        return commands.Reply(
            f"Location changed to {current_location}!",
            "location_changed"
        )

    @commands.register_meta
    def debug(location, *args):
        """Toggle the debug state of the story in the current location."""
        app = commands.get_app_in(location)
        if app is None:
            return commands.Reply(
                "No story is being played in the current location!",
                "debug_no_story"
            )
        app.debug = not app.debug
        return commands.Reply(
            f"Debug is now {app.debug}!",
            "debug"
        )

    while True:
        message = input(f"{current_location}: ")

        # To exit the program.
        if message.lower() in ("q", "quit"):
            sys.exit(0)

        reply = commands.parse_command(current_location, message)

        # Echo back inputs that are not a command.
        if reply.id == commands.Reply.PARSE_NOT_A_COMMAND:
            print(message)
            continue

        # Or show the response to the command.
        new_status = get_playing_status()
        global last_playing_status
        if new_status != last_playing_status:
            last_playing_status = new_status
            game = new_status or "nothing"
            print(f"New status: playing {game}")
        if reply.id == commands.Reply.PLAY_STARTED:
            app = commands.get_app_in(current_location)
            cover_image = find_cover(app.story)
            if cover_image is not None:
                print(f"Cover image: {cover_image}")
        for output in process_bot_output(reply.message, hyperlinks=commands.show_hyperlinks):
            print(output)
        if reply.id:
            print("reply.id:", reply.id)
