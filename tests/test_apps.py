from unittest import mock

from xyzzanie import apps
from xyzzanie import glk

from .simpleapp import SimpleApp


# These tests are taken from the Python library (`test_list2cmdline`).
# (See https://github.com/python/cpython/blob/3.8/Lib/test/test_subprocess.py)
def test_join_command_windows():
    """Test for the `join_command_window` function."""
    assert apps.join_command_windows(["a b c", "d", "e"]) == '"a b c" d e'
    assert apps.join_command_windows(['ab"c', "\\", "d"]) == 'ab\\"c \\ d'
    assert apps.join_command_windows(['ab"c', " \\", "d"]) == 'ab\\"c " \\\\" d'
    assert apps.join_command_windows(["a\\\\\\b", "de fg", "h"]) == 'a\\\\\\b "de fg" h'
    assert apps.join_command_windows(['a\\"b', "c", "d"]) == 'a\\\\\\"b c d'
    assert apps.join_command_windows(["a\\\\b c", "d", "e"]) == '"a\\\\b c" d e'
    assert apps.join_command_windows(["a\\\\b\\ c", "d", "e"]) == '"a\\\\b\\ c" d e'
    assert apps.join_command_windows(["ab", ""]) == 'ab ""'


def test_filename_is_safe():
    """Test the `filename_is_safe` function."""
    assert not apps.filename_is_safe("aze rty")
    assert not apps.filename_is_safe("aze:rty")
    assert not apps.filename_is_safe("aze\\rty")
    assert not apps.filename_is_safe("aze;rty")
    assert not apps.filename_is_safe("aze'rty")
    assert not apps.filename_is_safe('aze"rty')


class TestLoadsMultipleObjectJson():
    """Tests for the `loads_multiple_object_json` function"""

    def test_empty_string(self):
        assert apps.loads_multiple_object_json("") == []

    def test_one_object(self):
        assert apps.loads_multiple_object_json('{"a" : 1}') == [{"a": 1}]

    def test_multiple_objects(self):
        arg = '{"a": 1} {"b": 2}'
        assert apps.loads_multiple_object_json(arg) == [{"a": 1}, {"b": 2}]


class TestBaseApp:
    """Test for the `BaseApp` class.

    Since an app class needs a `_accept` method, which `BaseApp` does not have,
    the subclass `SimpleApp` will be used at times.
    """

    def test_init(self):
        app = apps.BaseApp(debug=True)
        assert app.debug == True  # noqa: E712
        assert app.stopped == False  # noqa: E712
        assert app.generation == 0
        assert app.windows == {}
        assert app.inputs == []
        assert app.metrics == {"width": 80, "height": 24}
        assert app.last_error == {}
        assert app.last_update == {}

    def test_init_without_debug(self):
        app = apps.BaseApp()
        assert app.debug == False  # noqa: E712

    def test_terminate(self):
        app = apps.BaseApp()
        app.terminate()
        assert app.stopped == True  # noqa: E712

    def test_error(self):
        app = apps.BaseApp()
        app.terminate = mock.MagicMock(name="app.terminate")
        app.error("An error occurred.")
        app.terminate.assert_called_once()
        assert app.last_error == glk.error_event("An error occurred.")

    def test_build_input_with_no_inputs(self):
        app = apps.BaseApp()
        result = app.build_input("xyzzy")
        assert result is apps.BuildInputResult.NO_INPUTS

    def test_build_input_with_only_hyperlinks(self):
        app = SimpleApp()
        app.build_input("hyperlinkonly")
        result = app.build_input("xyzzy")
        assert result is apps.BuildInputResult.ONLY_HYPERLINKS

    def test_build_input_line(self):
        app = SimpleApp()
        result = app.build_input("xyzzy")
        assert result is apps.BuildInputResult.SUCCESS
        new_content = app.output().buffer_windows[0][0].content
        assert new_content == "xyzzy"

    def test_build_input_char(self):
        app = SimpleApp()
        tested_chars = {
            "x": "x",  # A single letter.
            "xyzzy": "x",  # A command.
            "": " ",  # An empty command.
            "return": "return"  # A special key.
        }
        for sent, expected in tested_chars.items():
            app.build_input("char")  # Make the app wait for a char.
            result = app.build_input(sent)
            assert result is apps.BuildInputResult.SUCCESS
            new_content = app.output().buffer_windows[0][-1].content
            assert new_content == f"char {expected}"

    def test_click_hyperlink_with_no_inputs(self):
        app = apps.BaseApp()
        result = app.click_hyperlink(0)
        assert result is apps.HyperlinkResult.NO_INPUT

    def test_click_hyperlink_with_invalid_value(self):
        app = SimpleApp()
        app.build_input("hyperlinkonly")
        result = app.click_hyperlink(-1)
        assert result is apps.HyperlinkResult.INVALID_VALUE

    def test_click_hyperlink_success(self):
        app = SimpleApp()
        app.build_input("hyperlinkonly")
        result = app.click_hyperlink(0)
        assert result is apps.HyperlinkResult.SUCCESS

    def test_accept_when_story_stopped(self):
        app = apps.BaseApp()
        app.terminate()
        app.accept({
            "type": "init",
            "gen": 0,
            "support": ["hyperlinks"],
            "metrics": app.metrics
        })
        assert app.generation == 0  # The generation did not increase.

    def test_accept_error_update(self):
        app = SimpleApp()
        app.accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "error"
        })
        assert app.stopped
        assert app.last_error == glk.error_event("Fatal error.")

    def test_accept_invalid_update(self):
        app = SimpleApp()
        app.accept({"type": "", "gen": 1})
        assert app.last_error["message"] == "Error without message."

    def test_accept_no_input_in_update(self):
        app = SimpleApp()
        app.terminate = mock.MagicMock(name="app.terminate")
        app.accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "noinputs"
        })
        app.terminate.assert_called_once()

    def test_accept_multiple_inputs_in_update(self):
        app = SimpleApp()
        app.accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "multipleinputs"
        })
        assert len(app.inputs) == 2
        assert app.last_error
        assert app.stopped

    def test_accept_all_windows_closed_in_update(self):
        app = SimpleApp()
        app.accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "closeall"
        })
        assert app.last_error
        assert app.stopped

    def test_accept_new_window_in_update(self):
        app = SimpleApp()
        app.accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "opengrid"
        })
        assert len(app.windows) == 2
        assert isinstance(app.windows[1], glk.GridWindow)

    def test_accept_closed_window_in_update(self):
        app = SimpleApp()
        app.accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "opengrid"
        })
        assert len(app.windows) == 2
        assert isinstance(app.windows[1], glk.GridWindow)
        app.accept({
            "type": "line",
            "gen": 2,
            "window": 0,
            "value": "closegrid"
        })
        assert len(app.windows) == 1

    def test_accept_updated_window_in_update(self):
        app = SimpleApp()
        assert app.windows[0].left == 0
        app.accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "updatewindow"
        })
        assert app.windows[0].left == 1

    def test_accept_last_update_set(self):
        captured_value = None

        def capture_returned_value(f):
            """Decorator retrieving the return value of the argument."""
            def new_function(*args, **kwargs):
                nonlocal captured_value
                captured_value = f(*args, **kwargs)
                return captured_value
            return new_function

        app = SimpleApp()
        app._accept = capture_returned_value(app._accept)
        app.accept({
            "type": "line",
            "gen": 1,
            "window": 0,
            "value": "xyzzy"
        })
        assert app.last_update == captured_value


class TestInterpreterApp:
    """Tests for the `InterpreterApp` class."""

    # TODO
    # def test_refresh_stories_available(self):
    #     pass

    def test_unknown_story_format(self):
        app = apps.InterpreterApp("test.azerty")
        assert app.stopped

    # TODO
    # def test_cannot_launch_process(self):
    #     pass
