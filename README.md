# Xyzzanie

Xyzzanie is a Discord bot written in Python allowing you to play parser interactive fictions that run on various virtual machines (including the Z-machine and Glulx). Note that you'll have to host it yourself to be able to use it.

**Warning: Xyzzanie hasn't been updated for some time, won't be for the foreseeable future, and very likely doesn't work anymore with Discord's latest API. Working on this project is not in my priorities right now, but if you need it and can't find a suitable alternative, get in touch and we'll see what can be done. By the way, one of the things I would do if I worked on it again would be to make it client-agnostic (see [#8](https://gitlab.com/Natrium729/xyzzanie/-/issues/8)).**

## Installation

The bot was written with Python 3.8. It may work with an earlier version, but is incompatible with Python 2.

You'll need to install the dependencies by typing the following in a terminal.

```
$ python -m venv .venv
$ pip install -r requirements.txt
```

Alternatively, if you have Poetry installed, you can use:

```
$ poetry install --no-dev --no-root
```

You'll also need interpreters compiled with [RemGlk](https://github.com/erkyrath/remglk). You'll have to compile them yourself or ask someone to do it for you. The source of various interpreters are [available on this GitHub repository](https://github.com/cspiegel/terps). You'll probably want Bocfel for Z-machine and git for Glulx.

*Note: I had trouble compiling some of them, in particular Glulxe. I could compile Glulxe [with this source](https://github.com/erkyrath/glulxe) however. On Windows, the interpreters have to be compiled with [Cygwin](https://cygwin.com/). If you need help, you could try the [intfiction.org forum](https://intfiction.org/c/technical-development/interpreters), where I'll likely answer.*

Once it's done, place the interpreters' executables in the `interpreters` folder, and your story files in the `stories` folder (the name of the story files should not contain spaces). By default, Xyzzanie will use Bocfel for Z-machine stories and git for Glulx stories. To use other interpreters or for other story formats, see the "Configure the bot" section below.

*Note: On Windows, you'll also need to copy `cygwin1.dll`, found in your Cygwin installation, alongside the interpreters.*

To test if everything is working before creating your Discord bot, you can run the bot locally in a terminal by using this command:

```
$ python xyzzanie.py sim
```

If everything goes well, you'll be able to interact with the bot as if you were on Discord (see "Interacting with the bot" below for how). When you're finished, type `q` to exit.

The bot was tested on macOS, Linux and Windows. Everything worked as expected, but there may be differences between OSes. If you encounter one, please file a bug!

## Running your bot on Discord

1. First of all, you need to have a Discord account. Log in [on their website](https://discordapp.com/).
2. Go to the [developper section of the site](https://discordapp.com/developers/applications/me) and click on "New App". Give it a name, a description and, optionnaly, an avatar.
3. On the bot's page, click on "Create a Bot User" and confirm.
4. On the same page in the "bot" section, click on "click to reveal" next to "Token". Copy it and paste it in a file named `secrets.json`, created in the same folder than `xyzzanie.py`. This file should look like the following:

```
{
    "token": "PasteYourTokenHere"
}
```

5. Invite the bot to a Discord server. If you have the right to manage the server, you can use the link `https://discordapp.com/oauth2/authorize?client_id=INSERT_CLIENT_ID_HERE&scope=bot&permissions=0`, where you replace `INSERT_CLIENT_ID_HERE` with your bot's client ID, as found at the top of its page.

Once all this is done, all you need is to launch your bot with this command:

```
$ python xyzzanie.py run
```

Each channel on every server the bot was invited will be able to play independantly to the stories in the `stories` folder without interfering with each other. I however don't know how well the performance will be if many game sessions are running at the same time.

When the script is interrupted (for example if you close the terminal window or shut down your computer), all game progress is lost.

If you want a more permanent solution, you'll have to host it on a server somewhere (like Heroku); you'll have to do some research on your own to know how to do it.

**DISCLAIMER: Since the bot launches processes on the machine it is installed, there could be security issues where Discord users could access to your computer. There should not be any, but I will not be held responsible if something happens.**

## Interacting with the bot

To interact with the bot, you have to prefix your Discord message with `!>>` to launch stories and perform other meta actions, and `!>` to submit a command to a game. (You can change these prefixes; see "Configure the bot" below).

### Getting help

Type `!>>help` to get a brief summary on how to interact with the bot, and to get a link to this readme.

### Getting informations about the bot

Type `!>>about` to get informations about the bot, such as its author and the repository link.

### Listing the stories available

Type `!>>list` to get in a private message the list of the stories that can be played.

### Refreshing the story list

Type `!>>refresh` if you added or removed stories in the `stories` folder while the bot was running. If you don't do that, people won't be able to launch newly added stories.

### Playing a story

Type `!>>play [storyfile]` to play the specified story file, as written in the list produced by `!>>list`.

It is not currently possible to save your progress or to upload a savefile, so you'll have to complete it in one run.

Also, please quit the story after playing it (usually by typing `!>quit`), whether you finished it or not. If you don't do that, it will leave a process running uselessly, which will take memory.

If a JPG or PNG picture with the same name as the story is present in the `stories` folder, it will be displayed when the story is launched. This feature is useful for cover arts.

### Forcequitting a story

Type `!>>forcequit` to immediatly end the story currently being played. Do that if you encounter a problem. Otherwise, just try quitting the story normally by typing `!>quit`.

### Interacting with a story

Type `!>` followed by your command to interact with the story being played; `!>get lamp` for instance.

If the game is not displaying a prompt, that could mean it is waiting for a single keypress. In that case, juste type an empty command (`!>`) to move the story forward. If a command is typed, then only the first character will be submitted as the key pressed, except if the command is one of the following:

* `left`
* `right`
* `up`
* `down`
* `return`
* `delete`
* `escape`
* `tab`
* `pageup`
* `pagedown`
* `home`
* `end`
* `func1`
* `func2`
* `func3`
* `func4`
* `func5`
* `func6`
* `func7`
* `func8`
* `func9`
* `func10`
* `func11`
* `func12`

In those cases, the named key will be pressed instead.

The key pressed is not important in a vast majority of games, so just enter an empty command `!>` when no prompt is displayed and you'll be good most of the time.

Some stories also have hyperlinks that trigger in-game commands. They are shown underlined with a number following them; for instance, "You could try to __jump__\[21\]". In that case, you can use `!>>link 21` to "click" it.

If you don't want the links to be indicated in the bot's messages, you can type `!>>link off`; they will be shown as regular text and without a number. To show them again, use `!>>link on`.

## Configuring the bot

The file `config.json` allows you to configure the bot. By default, it is empty. In that case, the bot will use the following settings:

```json
{
    "meta_prompt": "!>>",
    "story_prompt": "!>",
    "terps": {
        ".z5": "bocfel",
        ".z8": "bocfel",
        ".zblorb": "bocfel",
        ".ulx": "git",
        ".gblorb": "git",
    },
    "language": "en",
}
```

You only need to specify the fields you want to change. For example, if you want to change the prompts, this should be written in `config.json`:

```json
{
    "meta_prompt": "xyzzanie>>",
    "story_prompt": "xyzzanie>",
}
```

If you also want to change the interpreter used for the Z-machine:

```json
{
    "meta_prompt": "xyzzanie>>",
    "story_prompt": "xyzzanie>",
    "terps": {
        ".z5": "frotz",
        ".z8": "frotz",
        ".zblorb": "frotz",
    }
}
```

There are only two restrictions:

* The story prompt can't start with the meta prompt. The story prompt can't be `!>>` if the meta prompt is `!>`, for instance.
* The ony languages supported are `"en"` for English and `"fr"` for French. If you want to translate the bot, please get in touch!

## Developing the bot

You'll need to install the development dependencies (which include flake8 and pytest):

```
$ python -m venv .venv
$ pip install -r requirements-dev.txt
```

Or, if using Poetry:

```
$ poetry install --no-root
```

You can launch the bot in dev mode by using the `-D` argument (e.g. `python xyzzanie.py -D sim`).

This will cause the bot to use `config-dev.json` as the config file, `interpreters-dev` and `stories-dev` as the interpreter and story folders, and the field `"token-dev"` in `secrets.json` as the Discord token.

## Licence

Xyzzanie is released under the MIT License. Please see `LICENSE.txt`.

## Xyzzanie?

[xyzzy](https://en.wikipedia.org/wiki/Xyzzy_(computing)) + [zizanie](https://en.wiktionary.org/wiki/zizanie)
