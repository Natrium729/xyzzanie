from unittest import mock

import pytest

from xyzzanie import apps
from xyzzanie import commands
from xyzzanie import config


class TestReply:
    """Tests for the `Reply` class."""

    def test_new_reply(self):
        reply = commands.Reply("My message", "my_id")
        assert reply.message == "My message"
        assert reply.id == "my_id"

    def test_str_reply(self):
        reply = commands.Reply("My message", "my_id")
        assert str(reply) == "My message"


@mock.patch.dict(commands.running_apps, clear=True)
def test_location():
    location = "test"
    app = apps.BaseApp()
    assert not commands.app_running_in(location)
    commands.add_app_to_location(app, location)
    assert commands.app_running_in(location)
    with pytest.raises(ValueError):
        commands.add_app_to_location(app, location)
    commands.remove_app_from_location(location)
    assert not commands.app_running_in(location)


def test_meta_commands_dict():
    assert commands.meta_commands["help"] == commands.get_help
    assert commands.meta_commands["about"] == commands.about
    assert commands.meta_commands["refresh"] == commands.refresh
    assert commands.meta_commands["list"] == commands.list_stories
    assert commands.meta_commands["play"] == commands.play
    assert commands.meta_commands["link"] == commands.link
    assert commands.meta_commands["forcequit"] == commands.forcequit
    assert commands.meta_commands["xyzzy"] == commands.xyzzy
    assert len(commands.meta_commands) == 8


class TestHandleMetaCommand:
    """Tests for `commands.handle_meta_command`."""

    def test_empty_meta(self):
        """When the meta command is empty."""
        reply = commands.handle_meta_command("location", "")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.META_EMPTY

    def test_invalid_meta_command(self):
        reply = commands.handle_meta_command(
            "location",
            f"{config.meta_prompt}azerty"
        )
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.META_UNKNOWN

    @mock.patch.dict(
        commands.meta_commands,
        {"test": mock.MagicMock(name="commands.meta_commands['test']")},
        clear=True
    )
    def test_valid_meta_command(self):
        commands.meta_commands["test"].return_value = commands.Reply(
            "Mocked!",
            "mocked_id"
        )
        reply = commands.handle_meta_command("location", "test")
        commands.meta_commands["test"].assert_called_once()
        assert isinstance(reply, commands.Reply)
        assert reply.message == "Mocked!"
        assert reply.id == "mocked_id"

    @mock.patch.dict(
        commands.meta_commands,
        {"test": mock.MagicMock(name="commands.meta_commands['test']")},
        clear=True
    )
    def test_valid_meta_command_no_reply(self):
        commands.meta_commands["test"].return_value = "No reply!"
        reply = commands.handle_meta_command("location", "test")
        commands.meta_commands["test"].assert_called_once()
        assert isinstance(reply, commands.Reply)
        assert reply.message == "No reply!"
        assert reply.id == commands.Reply.META_NO_REPLY


@mock.patch.dict(commands.running_apps, clear=True)
class TestHandleStoryCommand:
    """Tests for `commands.handle_story_command`."""

    def test_story_not_playing(self):
        reply = commands.handle_story_command("location", "")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.STORY_NOT_PLAYING

    def test_story_stopped(self):
        location = "location"
        app = apps.BaseApp()
        app.build_input = mock.MagicMock(name="app.build_input")
        app.build_input.return_value = apps.BuildInputResult.STORY_STOPPED
        commands.add_app_to_location(app, location)
        reply = commands.handle_story_command(location, "")
        app.build_input.assert_called_once()
        assert reply.id == commands.Reply.STORY_STOPPED

    def test_story_no_inputs(self):
        location = "location"
        app = apps.BaseApp()
        app.build_input = mock.MagicMock(name="app.build_input")
        app.build_input.return_value = apps.BuildInputResult.NO_INPUTS
        commands.add_app_to_location(app, location)
        reply = commands.handle_story_command(location, "")
        app.build_input.assert_called_once()
        assert reply.id == commands.Reply.STORY_NO_INPUTS

    def test_story_only_hyperlinks(self):
        location = "location"
        app = apps.BaseApp()
        app.build_input = mock.MagicMock(name="app.build_input")
        app.build_input.return_value = apps.BuildInputResult.ONLY_HYPERLINKS
        commands.add_app_to_location(app, location)
        reply = commands.handle_story_command(location, "")
        app.build_input.assert_called_once()
        assert reply.id == commands.Reply.STORY_ONLY_HYPERLINKS

    def test_story_output(self):
        location = "location"
        app = apps.BaseApp()
        app.stopped = False
        app.build_input = mock.MagicMock(name="app.build_input")
        app.build_input.return_value = apps.BuildInputResult.SUCCESS
        app.output = mock.MagicMock(name="app.output")
        commands.add_app_to_location(app, location)
        reply = commands.handle_story_command(location, "")
        app.build_input.assert_called_once()
        app.output.assert_called_once()
        assert reply.id == commands.Reply.STORY_OUTPUT

    def test_story_output_and_ended(self):
        location = "location"
        app = apps.BaseApp()
        app.stopped = True
        app.build_input = mock.MagicMock(name="app.build_input")
        app.build_input.return_value = apps.BuildInputResult.SUCCESS
        app.output = mock.MagicMock(name="app.output")
        commands.add_app_to_location(app, location)
        reply = commands.handle_story_command(location, "")
        app.build_input.assert_called_once()
        app.output.assert_called_once()
        assert reply.id == commands.Reply.STORY_ENDED


class TestParseCommand:
    """Tests for `commands.parse_command`."""

    @mock.patch("xyzzanie.commands.handle_meta_command")
    def test_meta_command(self, mocked_handle_meta):
        prompt = config.meta_prompt
        tested_commands = (prompt, f"{prompt}qwerty", f"{prompt}help")
        mocked_handle_meta.return_value = "mocked_return_value"
        for command in tested_commands:
            result = commands.parse_command("location", command)
            mocked_handle_meta.assert_called_once()
            assert result == "mocked_return_value"
            mocked_handle_meta.reset_mock()

    @mock.patch("xyzzanie.commands.handle_story_command")
    def test_story_command(self, mocked_handle_story):
        prompt = config.story_prompt
        tested_commands = (prompt, f"{prompt}look", f"{prompt}get lamp")
        mocked_handle_story.return_value = "mocked_return_value"
        for command in tested_commands:
            result = commands.parse_command("location", command)
            mocked_handle_story.assert_called_once()
            assert result == "mocked_return_value"
            mocked_handle_story.reset_mock()

    def test_not_a_command(self):
        reply = commands.parse_command("location", "azerty")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.PARSE_NOT_A_COMMAND


class TestHelp:
    """Tests for the help command."""

    def test_help(self):
        reply = commands.get_help("location")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.HELP

    def test_help_with_args(self):
        reply = commands.get_help("location", "useless arg")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.HELP


class TestAbout:
    """Tests for the about command."""

    def test_about(self):
        reply = commands.about("location")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.ABOUT

    def test_about_with_args(self):
        reply = commands.about("location", "useless arg")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.ABOUT


class TestRefresh:
    """Tests for the refresh command."""

    def test_refresh_success(self):
        apps.InterpreterApp.refresh_stories_available = mock.MagicMock(
            name="apps.InterpreterApp.refresh_stories_available"
        )
        apps.InterpreterApp.refresh_stories_available.return_value = (
            apps.RefreshStoriesResult.SUCCESS
        )
        reply = commands.refresh("location")
        apps.InterpreterApp.refresh_stories_available.assert_called_once()
        assert reply.id == commands.Reply.REFRESH_OK

    def test_refresh_folder_missing(self):
        apps.InterpreterApp.refresh_stories_available = mock.MagicMock(
            name="apps.InterpreterApp.refresh_stories_available"
        )
        apps.InterpreterApp.refresh_stories_available.return_value = (
            apps.RefreshStoriesResult.FOLDER_MISSING
        )
        reply = commands.refresh("location")
        apps.InterpreterApp.refresh_stories_available.assert_called_once()
        assert reply.id == commands.Reply.REFRESH_MISSING

    def test_refresh_error(self):
        apps.InterpreterApp.refresh_stories_available = mock.MagicMock(
            name="apps.InterpreterApp.refresh_stories_available"
        )
        apps.InterpreterApp.refresh_stories_available.return_value = (
            apps.RefreshStoriesResult.ERROR
        )
        reply = commands.refresh("location")
        apps.InterpreterApp.refresh_stories_available.assert_called_once()
        assert reply.id == commands.Reply.REFRESH_ERROR

    def test_refresh_success_with_args(self):
        apps.InterpreterApp.refresh_stories_available = mock.MagicMock(
            name="apps.InterpreterApp.refresh_stories_available"
        )
        apps.InterpreterApp.refresh_stories_available.return_value = (
            apps.RefreshStoriesResult.SUCCESS
        )
        reply = commands.refresh("location", "useless arg")
        apps.InterpreterApp.refresh_stories_available.assert_called_once()
        assert reply.id == commands.Reply.REFRESH_OK


class TestList:
    """Tests for the list command."""

    def test_list(self):
        reply = commands.list_stories("location")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.LIST

    def test_list_with_args(self):
        reply = commands.list_stories("location", "useless arg")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.LIST


@mock.patch.dict(commands.running_apps, clear=True)
class TestPlay:
    """Tests for the play command."""

    def test_play_already_playing(self):
        location = "location"
        app = apps.BaseApp()
        commands.add_app_to_location(app, location)
        reply = commands.play(location, "adventure.z5")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.PLAY_ALREADY_PLAYING

    def test_play_no_arg(self):
        reply = commands.play("location")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.PLAY_NO_STORY_GIVEN

    @mock.patch.object(apps.InterpreterApp, "stories_available", [])
    def test_play_story_not_in_list(self):
        reply = commands.play("location", "adventure.z5")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.PLAY_NOT_IN_LIST

    # TODO
    # def test_play_story_unopenable(self):
    #     pass

    # TODO
    # def test_play_story_nonexistent_file(self):
    #     pass

    # TODO
    # def test_play_app_unlaunchable(self):
    #     pass

    # TODO
    # def test_play_success(self):
    #     pass


@mock.patch.dict(commands.running_apps, clear=True)
class TestLink:
    """Tests for the link command."""

    def test_link_no_args(self):
        reply = commands.link("location")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.LINK_NO_ARGS

    @mock.patch.dict(commands.running_apps, {"location": apps.BaseApp()})
    def test_link_invalid_arg(self):
        reply = commands.link("location", "abc")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.LINK_INVALID_ARG

    def test_link_on(self):
        reply = commands.link("location", "on")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.LINK_ENABLE
        # And now with capitalisation.
        reply = commands.link("location", "oN")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.LINK_ENABLE

    def test_link_off(self):
        reply = commands.link("location", "off")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.LINK_DISABLE
        # And now with capitalisation.
        reply = commands.link("location", "OFf")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.LINK_DISABLE

    def test_link_no_story_playing(self):
        reply = commands.link("location", "729")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.STORY_NOT_PLAYING

    @mock.patch.dict(commands.running_apps, {"location": apps.BaseApp()})
    def test_link_story_no_inputs(self):
        location = "location"
        app = commands.get_app_in(location)
        app.click_hyperlink = mock.MagicMock(name="app.click_hyperlink")
        app.click_hyperlink.return_value = apps.HyperlinkResult.NO_INPUT
        reply = commands.link(location, "1")
        app.click_hyperlink.assert_called_once()
        assert reply.id == commands.Reply.LINK_NO_INPUTS

    @mock.patch.dict(commands.running_apps, {"location": apps.BaseApp()})
    def test_link_story_invalid_value(self):
        location = "location"
        app = commands.get_app_in(location)
        app.click_hyperlink = mock.MagicMock(name="app.click_hyperlink")
        app.click_hyperlink.return_value = apps.HyperlinkResult.INVALID_VALUE
        reply = commands.link(location, "1")
        app.click_hyperlink.assert_called_once()
        assert reply.id == commands.Reply.LINK_INVALID_VALUE

    @mock.patch.dict(commands.running_apps, {"location": apps.BaseApp()})
    def test_link_success(self):
        location = "location"
        app = commands.get_app_in(location)
        app.stopped = False
        app.click_hyperlink = mock.MagicMock(name="app.click_hyperlink")
        app.click_hyperlink.return_value = apps.HyperlinkResult.SUCCESS
        app.output = mock.MagicMock(name="app.output")
        reply = commands.link(location, "1")
        app.click_hyperlink.assert_called_once()
        assert reply.id == commands.Reply.STORY_OUTPUT

    @mock.patch.dict(commands.running_apps, {"location": apps.BaseApp()})
    def test_link_success_story_ended(self):
        location = "location"
        app = commands.get_app_in(location)
        app.stopped = True
        app.click_hyperlink = mock.MagicMock(name="app.click_hyperlink")
        app.click_hyperlink.return_value = apps.HyperlinkResult.SUCCESS
        app.output = mock.MagicMock(name="app.output")
        reply = commands.link(location, "1")
        app.click_hyperlink.assert_called_once()
        assert reply.id == commands.Reply.STORY_ENDED


@mock.patch.dict(commands.running_apps, clear=True)
class TestForcequit:
    """Tests for the forcequit command."""

    def test_forcequit_no_story_playing(self):
        reply = commands.forcequit("location")
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.FORCEQUIT_WITHOUT_STORY

    def test_forcequit(self):
        location = "location"
        app = apps.BaseApp()
        app.terminate = mock.MagicMock(name="app.terminate")
        commands.add_app_to_location(app, location)
        reply = commands.forcequit("location")
        app.terminate.assert_called_once
        assert not commands.app_running_in(location)
        assert isinstance(reply, commands.Reply)
        assert reply.id == commands.Reply.FORCEQUIT
