from .en import en
from .fr import fr

languages = {
    "en": en,
    "fr": fr,
}
