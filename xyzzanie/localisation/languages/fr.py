fr = {
    # Playing status when playing two apps.
    "playing_status_two_apps": "${last_app} et 1 autre",

    # Playing status when playing more than two apps.
    "playing_status_more_apps": "${last_app} et ${other_number} autres",

    # The bot returned no reply to a command.
    "no_reply_returned": "Une erreur s'est produite. Veuillez essayer autre chose.",

    # An empty meta command was sent to the bot.
    "meta_command_empty": 'Tapez une commande après « ${meta_prompt} ». Pour la liste des commandes disponibles, tapez « ${meta_prompt}help ».',

    # A meta command didn't return a reply.
    "meta_command_no_reply": "La commande a été exécutée, mais aucune réponse n'a été renvoyée.",

    # Invalid meta command.
    "meta_command_invalid": 'La commande « ${command} » est invalide. Pour connaître les commandes disponibles, tapez « ${meta_prompt}help ».',

    # A story command is sent when no story is being played.
    "story_not_playing": "Aucune histoire n'est en train d'être jouée. Tapez « ${meta_prompt}list » pour obtenir la liste des histoires disponibles, et « ${meta_prompt}play [nom du fichier] » pour jouer une histoire donnée.",

    # A story command is sent when the story is stopped.
    "story_is_stopped": "ERREUR : L'histoire est arrêtée. Tapez « ${meta_prompt}forcequit » pour l'enlever et pouvoir ensuite lancer une autre histoire.",

    # An input is sent to a story that awaits only hyperlinks.
    "story_only_hyperlinks": "L'histoire n'attend que des hyperliens. Tapez « ${meta_prompt}link [nombre] » pour cliquer sur un hyperlien.",

    # An input is sent to a story that awaits none.
    "story_no_inputs": "ERREUR: L'histoire n'attend aucune entrée.",

    # The help text.
    "help":
        "Tapez « ${meta_prompt}list » pour obtenir la liste des histoires disponibles et « ${meta_prompt}play [nom du fichier] » pour jouer une histoire donnée.\nVous pouvez ensuite interagir avec l'histoire en préfixant votre commande avec « ${story_prompt} ».\nPour plus d'informations à propos de ce bot, tapez « ${meta_prompt}about ».\nPour une aide détaillée, la liste complète des commandes et le code source, visitez <https://gitlab.com/Natrium729/xyzzanie>.",

    # The about text.
    "about":
        "Xyzzanie est un bot Discord écrit par Nathanaël Marion (alias Natrium729).\nPour plus d'informations, visitez <https://gitlab.com/Natrium729/xyzzanie>.",

    # The story list has been refreshed.
    "story_list_refreshed": "La liste des histoires a été actualisée.",

    # The story folder doesn't exist.
    "story_folder_missing": "ERREUR: Le dossier d'histoires n'existe pas.",

    # The story folder cannot be opened.
    "story_folder_unopenable": "ERREUR: Le dossier d'histoires n'a pas pu être ouvert.",

    # The title when listing stories.
    "story_list_title": "Histoires disponibles",

    # A story is already being played when trying to start a new one.
    "story_already_playing": "L'histoire « ${story} » est en train d'être jouée. Quittez-la avant de lancer une autre histoire. (Vous pouvez aussi forcer l'histoire en cours à quitter en tapant « ${meta_prompt}forcequit ».)",

    # No story specified when trying to start one.
    "no_story_given": "Veuillez donner un fichier d'histoire. (Tapez « ${meta_prompt}list » pour obtenir la liste des histoires disponibles.)",

    # The story is not in the list of stories.
    "story_not_in_list": "Le fichier d'histoire « ${filename} » n'a pas pu être trouvé. Veuillez taper « ${meta_prompt}list » pour obtenir la liste des histoires disponibles.",

    # The story file could not be opened.
    "story_unopenable": "ERREUR: La fichier d'histoire « ${filename} » n'a pas pu être ouvert.",

    # The interpreter couldn't be launched.
    "interpreter_unlaunchable": "ERREUR: L'interpréteur n'a pas pu être lancé.",

    # The story file doesn't exist.
    "story_file_nonexistent": "Le fichier d'histoire « ${filename} » n'existe pas. Veuillez taper « ${meta_prompt}refresh » pour actualiser la liste d'histoires.",

    # Link command with no arguments or an invalid arg.
    "link_invalid_arg": "Veuillez donner un numéro avec la commande pour cliquer sur le lien correspondant dans l'histoire en cours. Vous pouvez aussi taper « ${meta_prompt}link off » et « ${meta_prompt}link on » désactiver ou activer leur affichage dans les histoires.",

    # Links are enabled.
    "link_enable": "Les liens seront maintenant indiqués dans les histoires.",

    # Links are disabled.
    "link_disable": "Les liens ne seront plus indiqués dans les histoires.",

    # Invalid link value.
    "link_invalid_value": "L'histoire n'a aucun lien disponible avec cette valeur.",

    # A story is forcequit.
    "forcequit": "L'histoire en cours a été arrêtée.",

    # Forcequit when no story is playing.
    "forcequit_without_story": "Aucune histoire n'est en train d'être jouée.",

    # A story is awaiting several inputs
    "story_with_several_inputs": "L'histoire en cours est en train d'attendre plusieurs entrées, mais Xyzzanie ne supporte qu'une seule entrée à la fois. L'histoire a été arrêtée.",

    # A story closed all its windows.
    "story_all_windows_closed": "L'histoire a fermé toutes ses fenêtres ; elle a été arrêtée.",

    # A story ends.
    "story_ended": "L'histoire « ${story} » est terminée.",
}
