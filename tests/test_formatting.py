import pytest

from xyzzanie import formatting


def test_styles():
    """Test the values of the `formatting.Style` enum.

    It is important for the Glk styles that they have these values because
    they're the ones used by RemGlk. It's also important for the others
    because the values are used in dictionaries.
    """

    assert formatting.Style.X_NONE.value == "x_none"
    assert formatting.Style.X_GRID.value == "x_grid"
    assert formatting.Style.X_INFO.value == "x_info"
    assert formatting.Style.X_ERROR.value == "x_error"
    assert formatting.Style.X_MARKDOWN_BLOCK.value == "x_markdown_block"
    assert formatting.Style.NORMAL.value == "normal"
    assert formatting.Style.EMPHASIZED.value == "emphasized"
    assert formatting.Style.PREFORMATTED.value == "preformatted"
    assert formatting.Style.HEADER.value == "header"
    assert formatting.Style.SUBHEADER.value == "subheader"
    assert formatting.Style.ALERT.value == "alert"
    assert formatting.Style.NOTE.value == "note"
    assert formatting.Style.BLOCKQUOTE.value == "blockquote"
    assert formatting.Style.INPUT.value == "input"
    assert formatting.Style.USER1.value == "user1"
    assert formatting.Style.USER2.value == "user2"
    assert formatting.Style.HYPERLINK.value == "hyperlink"


class TestText:
    """Tests for the `Text` class"""

    def test_init(self):
        text = formatting.Text("Hello World", formatting.Style.NORMAL)
        assert text.content == "Hello World"
        assert text.style is formatting.Style.NORMAL

    def test_repr(self):
        text = formatting.Text("Hello World", formatting.Style.NORMAL)
        assert repr(text) == "Text('Hello World', Style.NORMAL, hyperlink=None)"
        text = formatting.Text("Hello World", formatting.Style.NORMAL, hyperlink=729)
        assert repr(text) == "Text('Hello World', Style.NORMAL, hyperlink=729)"

    def test_str(self):
        text = formatting.Text("Hello World", formatting.Style.NORMAL)
        assert str(text) == "Hello World"

    def test_eq(self):
        text_1 = formatting.Text("Hello World", formatting.Style.NORMAL)
        text_2 = formatting.Text("Hello World", formatting.Style.NORMAL)
        text_other_content = formatting.Text("Goodbye World", formatting.Style.NORMAL)
        text_other_style = formatting.Text("Hello World", formatting.Style.HEADER)
        text_other_hyperlink = formatting.Text("Hello World", formatting.Style.NORMAL, hyperlink=729)

        assert text_1 != "non-Text object"
        assert text_1 == text_2
        assert text_1 != text_other_content
        assert text_1 != text_other_style
        assert text_1 != text_other_hyperlink

    def test_to_markdown(self):
        for style in formatting.Style:
            text = formatting.Text("Hello World", style)
            assert text.to_markdown() == formatting.to_markdown("Hello World", text.style)

    def test_to_markdown_with_hyperlink(self):
        text = formatting.Text("Hello World", formatting.Style.EMPHASIZED, hyperlink=729)
        assert text.to_markdown() == "__*Hello World*__[729]"
        assert text.to_markdown(hyperlink=False) == "*Hello World*"


class TestToMarkdown:
    def test_applying_styles(self):
        expected = (
            (formatting.Style.NORMAL, "Hello World"),
            (formatting.Style.EMPHASIZED, "*Hello World*"),
            (formatting.Style.PREFORMATTED, "`Hello World`"),
            (formatting.Style.HEADER, "__**Hello World**__"),
            (formatting.Style.SUBHEADER, "**Hello World**"),
            (formatting.Style.ALERT, "***Hello World***"),
            (formatting.Style.NOTE, "__Hello World__"),
            (formatting.Style.BLOCKQUOTE, "```\nHello World\n```"),
            (formatting.Style.INPUT, "**Hello World**"),
            (formatting.Style.USER1, "Hello World"),
            (formatting.Style.USER2, "Hello World"),
            (formatting.Style.X_INFO, "```diff\n+ Hello World\n```"),
            (formatting.Style.X_ERROR, "```diff\n- Hello World\n```"),
        )
        for style, result in expected:
            assert formatting.to_markdown("Hello World", style) == result

    def test_multiline_styles(self):
        text = "Hello\n\nWorld\n"
        expected_info = "```diff\n+ Hello\n\n+ World\n\n```"
        expected_error = "```diff\n- Hello\n\n- World\n\n```"
        assert formatting.to_markdown(text, formatting.Style.X_INFO) == expected_info
        assert formatting.to_markdown(text, formatting.Style.X_ERROR) == expected_error

    def test_escape_markdown(self):
        text = "*** You win ***"
        expected = "\\*\\*\\* You win \\*\\*\\*"
        assert formatting.to_markdown(text, formatting.Style.NORMAL) == expected

    def test_escape_mentions(self):
        text = "@everyone"
        expected = "@\u200beveryone"
        assert formatting.to_markdown(text, formatting.Style.NORMAL) == expected

    def test_dont_escape_preformatted(self):
        text = "*** You win ***"
        expected = "`*** You win ***`"
        assert formatting.to_markdown(text, formatting.Style.PREFORMATTED) == expected


class TestSplitMessage():
    """Tests for the `split_message` function."""

    def test_short_simple_message(self):
        message = (formatting.Text("Hello World", formatting.Style.NORMAL),)
        assert formatting.split_message(message, 1000) == ["Hello World"]

    def test_short_simple_message_with_middle_delimiter(self):
        message = (formatting.Text("Hello World", formatting.Style.X_INFO),)
        expected = ["```diff\n+ Hello World\n```"]
        assert formatting.split_message(message, 1000) == expected

    def test_short_styled_message(self):
        message = (formatting.Text("Hello World", formatting.Style.EMPHASIZED),)
        assert formatting.split_message(message, 1000) == ["*Hello World*"]

    def test_short_multi_styled_message(self):
        message = (
            formatting.Text("Hello ", formatting.Style.NORMAL),
            formatting.Text("World", formatting.Style.EMPHASIZED),
        )
        assert formatting.split_message(message, 1000) == ["Hello *World*"]

    def test_long_simple_message(self):
        message = (formatting.Text("Hello World", formatting.Style.NORMAL),)
        assert formatting.split_message(message, 6) == ["Hello ", "World"]

    def test_long_styled_message(self):
        message = (formatting.Text("Hello World", formatting.Style.EMPHASIZED),)
        assert formatting.split_message(message, 8) == ["*Hello *", "*World*"]

    def test_long_multi_styled_message(self):
        message = (
            formatting.Text("Hello ", formatting.Style.NORMAL),
            formatting.Text("World", formatting.Style.EMPHASIZED),
        )
        assert formatting.split_message(message, 12) == ["Hello *Worl*", "*d*"]

    def test_escape_markdown(self):
        message = (formatting.Text("*** You win ***", formatting.Style.NORMAL),)
        expected = ["\\*\\*\\* You win \\*\\*\\*"]
        assert formatting.split_message(message, 1000) == expected

    def test_escape_mentions(self):
        message = (formatting.Text("@everyone", formatting.Style.NORMAL),)
        assert formatting.split_message(message, 1000) == ["@\u200beveryone"]

    def test_dont_escape_preformatted(self):
        message = (formatting.Text("*** You win ***", formatting.Style.PREFORMATTED),)
        assert formatting.split_message(message, 1000) == ["`*** You win ***`"]

    def test_hyperlink(self):
        message = (formatting.Text("Hello World", formatting.Style.EMPHASIZED, hyperlink=729),)
        expected = ["__*Hello World*__[729]"]
        assert formatting.split_message(message, 1000) == expected

    def test_unformatted_hyperlink(self):
        message = (formatting.Text("Hello World", formatting.Style.EMPHASIZED, hyperlink=729),)
        expected = ["*Hello World*"]
        assert formatting.split_message(message, 1000, hyperlinks=False) == expected

    def test_max_length_too_small(self):
        message = (formatting.Text("Hello", formatting.Style.NORMAL),)
        with pytest.raises(ValueError):
            formatting.split_message(message, 0)
        message = (formatting.Text("Hello", formatting.Style.EMPHASIZED),)
        with pytest.raises(ValueError):
            formatting.split_message(message, 2)
